

                     H Y P L A S   ANALYSIS  RESULTS
                    =================================

        _________________________________________________________ 
       |                                                         |
       | Program compiled with the dimensioning parameters       |
       |_________________________________________________________|
       |                                                 |       |
       | Maximum number of elements              (MELEM) |  2000 |
       | Maximum frontwidth allowed in solution  (MFRON) |   300 |
       | Maximum number of element groups        (MGRUP) |    10 |
       | Maximum number of load increments       (MINCS) |   200 |
       | Maximum number of nodal points          (MPOIN) |  4000 |
       | Size of increment cutting stack array   (MSUBIN)|    10 |
       | Max. number of nodes with prescr.displ. (MVFIV) |   500 |
       |_________________________________________________|_______|


 Data file name:
 ===============

 simple_ogden.dat


 Title:
 ======

 FEAP * * 4-Element Patch Test                                           


 Analysis description:
 =====================

 Analysis type ....................................... =    2
        1 = Plane stress
        2 = Plane strain
        3 = Axisymmetric

 Large deformation flag .............................. =   ON

 Nonlinear solution algorithm ........................ =    2
        Negative for the arc length method
        1 = Initial stiffness method
        2 = Newton-Raphson tangential stiffness method
        3 = Modified Newton KT1
        4 = Modified Newton KT2
        5 = Secant Newton - Initial stiffness
        6 = Secant Newton - KT1
        7 = Secant Newton - KT2


 Element Groups:        Number of element groups =     1
 ===============

 Group     Element type    Material type

     1          1                 1


 Element types:          Number of element types =     1
 ==============

 Element type number   1
 -------------------
 QUAD_4 (standard 4-noded quadrilateral)
 Integration rule:  4 gauss points


 Material properties:        Number of materials =     1
 ====================

 Material type number  1
 --------------------
 Ogden type hyperelastic material

 Mass density ...................................... =    0.00000    

 Number of terms in Ogden's strain-energy function.. =  2

            mu                  alpha

         36.7000             2.00000    
        -2.93600            -2.00000    

 Bulk modulus ...................................... =    0.00000    



 Element connectivities:      Number of elements =     4
 =======================

 Elem.  Group             Node numbers

   1       1              1    2    5    4
   2       1              2    3    6    5
   3       1              4    5    8    7
   4       1              5    6    9    8


 Nodal point co-ordinates:       Number of nodes =     9
 =========================

 Node    X-Coord        Y-Coord

    1    0.00000        0.00000    
    2    4.00000        0.00000    
    3    10.0000        0.00000    
    4    0.00000        4.50000    
    5    5.50000        5.50000    
    6    10.0000        5.00000    
    7    0.00000        10.0000    
    8    4.20000        10.0000    
    9    10.0000        10.0000    


 Prescribed displacements:    Number of nodes with prescribed displacement =     6
 =========================

 Node   Code             Prescribed values                 Angle

    1     11       0.00000        0.00000        0.00000    
    4     10       0.00000        0.00000        0.00000    
    7     10       0.00000        0.00000        0.00000    
    3     10      0.937500        0.00000        0.00000    
    6     10      0.937500        0.00000        0.00000    
    9     10      0.937500        0.00000        0.00000    



 No Master/Slave nodal constraints specified
 ===========================================



 Maximum frontwidth encountered =   10



 Loading specification (other than prescribed displacements)
 ===========================================================

    If any of the flags below is set to 1,  then
    the corresponding type of loading is applied
    to the structure.

 Point loading flag ...........................=  0
 Gravity loading flag .........................=  0
 Distributed edge loading flag ................=  0



 Increment control with fixed load increments selected
 =====================================================

       Number of proportional load increments =   10





 INCREMENT NUMBER    1
 =====================


 Total load factor ............... =    0.100000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    1                   TOTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.107446                     0.201444E-02
        2                     0.100127E-05                 0.187618E-07
        3                     0.127472E-11                 0.239009E-13
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   1 Load factor =   0.100000    
 ===========================================================

 Converged solution at iteration number =    3


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.375000E-01  -0.169941E-16
    3     0.937500E-01  -0.269599E-16
    4      0.00000       0.211284E-01
    5     0.515625E-01   0.258236E-01
    6     0.937500E-01   0.234760E-01
    7      0.00000       0.469519E-01
    8     0.393750E-01   0.469519E-01
    9     0.937500E-01   0.469519E-01


 Reactions
 =========

 Node      X-Force          Y-Force
    1    -0.824451        -0.939093E-14
    3     0.916057          0.00000    
    4     -1.83211          0.00000    
    6      1.83211          0.00000    
    7     -1.00766          0.00000    
    9     0.916057          0.00000    
       ---------------  ---------------
 Totals  -0.111022E-15    -0.939093E-14


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9208     Y-Coord=   1.000    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.9455E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1485E-13
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  2       X-Coord=   1.106     Y-Coord=   3.733    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.3435E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.5397E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  3       X-Coord=   3.437     Y-Coord=   1.123    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.6044E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.9495E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  4       X-Coord=   4.126     Y-Coord=   4.191    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =   0.1702E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =   0.2674E-14
 S-eff =   0.6317     Press.=   0.2035E-15


 Element number    2

 Gauss point  1       X-Coord=   5.570     Y-Coord=   1.145    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.6012E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.9445E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  2       X-Coord=   6.259     Y-Coord=   4.274    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.5153E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.8096E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  3       X-Coord=   8.882     Y-Coord=   1.084    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.6012E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.9445E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  4       X-Coord=   9.066     Y-Coord=   4.046    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =   0.8517E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =   0.1338E-13
 S-eff =   0.6317     Press.=   0.2035E-15


 Element number    3

 Gauss point  1       X-Coord=   1.115     Y-Coord=   5.856    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.7964E-19 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1251E-16
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  2       X-Coord=  0.9545     Y-Coord=   8.924    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.5161E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.8108E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  3       X-Coord=   4.160     Y-Coord=   6.314    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =   0.1702E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =   0.2674E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  4       X-Coord=   3.562     Y-Coord=   9.047    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.3186E-18 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.5005E-16
 S-eff =   0.6317     Press.=   0.2035E-15


 Element number    4

 Gauss point  1       X-Coord=   6.293     Y-Coord=   6.398    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.6887E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1082E-13
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  2       X-Coord=   5.695     Y-Coord=   9.069    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.3451E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.5422E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  3       X-Coord=   9.075     Y-Coord=   6.169    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.8628E-17 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1356E-14
 S-eff =   0.6317     Press.=   0.2035E-15

 Gauss point  4       X-Coord=   8.915     Y-Coord=   9.008    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.6871E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1079E-13
 S-eff =   0.6317     Press.=   0.2035E-15


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.1268E-15 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1991E-13
 S-eff =   0.6317     Press.=   0.2405E-15

 Node number     2    X-Coord=   4.037     Y-Coord= -0.1699E-16
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.6178E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.9706E-14
 S-eff =   0.6317     Press.=   0.2405E-15

 Node number     3    X-Coord=   10.09     Y-Coord= -0.2696E-16
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.1316E-15 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.2068E-13
 S-eff =   0.6317     Press.=   0.2405E-15

 Node number     4    X-Coord=   0.000     Y-Coord=   4.521    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.8165E-17 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1283E-14
 S-eff =   0.6317     Press.=   0.2405E-15

 Node number     5    X-Coord=   5.552     Y-Coord=   5.526    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.3534E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.5552E-14
 S-eff =   0.6317     Press.=   0.2405E-15

 Node number     6    X-Coord=   10.09     Y-Coord=   5.023    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =   0.1274E-15 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =   0.2001E-13
 S-eff =   0.6317     Press.=   0.2220E-15

 Node number     7    X-Coord=   0.000     Y-Coord=   10.05    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.9383E-16 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1474E-13
 S-eff =   0.6317     Press.=   0.2405E-15

 Node number     8    X-Coord=   4.239     Y-Coord=   10.05    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =   0.9960E-17 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =   0.1565E-14
 S-eff =   0.6317     Press.=   0.2220E-15

 Node number     9    X-Coord=   10.09     Y-Coord=   10.05    
 S-xx  =   0.3647     S-yy  =   0.4724E-14 S-xy  =  -0.1159E-15 S-zz  =  -0.3647    
 S-max =   0.3647     S-min =   0.4718E-14 Angle =  -0.1820E-13
 S-eff =   0.6317     Press.=   0.2035E-15




 INCREMENT NUMBER    2
 =====================


 Total load factor ............... =    0.200000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    2                   TOTAL LOAD FACTOR =   0.200000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.532305E-01                 0.196793E-02
        2                     0.486692E-06                 0.179880E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   2 Load factor =   0.200000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.750000E-01  -0.176360E-16
    3     0.187500      -0.133470E-16
    4      0.00000       0.423254E-01
    5     0.103125       0.517310E-01
    6     0.187500       0.470282E-01
    7      0.00000       0.940564E-01
    8     0.787500E-01   0.940564E-01
    9     0.187500       0.940564E-01


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -1.62620         0.719521E-08
    3      1.80688          0.00000    
    4     -3.61377          0.00000    
    6      3.61377          0.00000    
    7     -1.98757          0.00000    
    9      1.80688          0.00000    
       ---------------  ---------------
 Totals   0.615064E-13     0.719521E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9294     Y-Coord=   1.005    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.1522E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.1218E-13
 S-eff =    1.240     Press.=  -0.2591E-14

 Gauss point  2       X-Coord=   1.116     Y-Coord=   3.751    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.2441E-16 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.1954E-14
 S-eff =    1.240     Press.=  -0.2591E-14

 Gauss point  3       X-Coord=   3.469     Y-Coord=   1.128    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.3038E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.2431E-13
 S-eff =    1.240     Press.=  -0.2591E-14

 Gauss point  4       X-Coord=   4.164     Y-Coord=   4.210    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.9992E-16 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.7996E-14
 S-eff =    1.240     Press.=  -0.2591E-14


 Element number    2

 Gauss point  1       X-Coord=   5.621     Y-Coord=   1.151    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.2812E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.2250E-13
 S-eff =    1.240     Press.=  -0.2591E-14

 Gauss point  2       X-Coord=   6.317     Y-Coord=   4.294    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.2001E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.1601E-13
 S-eff =    1.240     Press.=   0.7661E-14

 Gauss point  3       X-Coord=   8.964     Y-Coord=   1.089    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.8327E-16 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.6663E-14
 S-eff =    1.240     Press.=   0.7661E-14

 Gauss point  4       X-Coord=   9.150     Y-Coord=   4.065    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.1208E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.9670E-14
 S-eff =    1.240     Press.=   0.7661E-14


 Element number    3

 Gauss point  1       X-Coord=   1.125     Y-Coord=   5.884    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.4535E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.3629E-13
 S-eff =    1.240     Press.=   0.7661E-14

 Gauss point  2       X-Coord=  0.9634     Y-Coord=   8.966    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.1609E-14 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.1287E-12
 S-eff =    1.240     Press.=   0.7661E-14

 Gauss point  3       X-Coord=   4.198     Y-Coord=   6.343    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.1959E-14 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.1567E-12
 S-eff =    1.240     Press.=   0.7661E-14

 Gauss point  4       X-Coord=   3.595     Y-Coord=   9.089    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.9784E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.7829E-13
 S-eff =    1.240     Press.=   0.7661E-14


 Element number    4

 Gauss point  1       X-Coord=   6.351     Y-Coord=   6.428    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.1707E-14 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.1366E-12
 S-eff =    1.240     Press.=  -0.2554E-14

 Gauss point  2       X-Coord=   5.748     Y-Coord=   9.112    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.5426E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.4342E-13
 S-eff =    1.240     Press.=   0.7661E-14

 Gauss point  3       X-Coord=   9.160     Y-Coord=   6.198    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.4278E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.3423E-13
 S-eff =    1.240     Press.=   0.7661E-14

 Gauss point  4       X-Coord=   8.998     Y-Coord=   9.050    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.2211E-14 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.1769E-12
 S-eff =    1.240     Press.=   0.7661E-14


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.1334E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.1067E-13
 S-eff =    1.240     Press.=  -0.2591E-14

 Node number     2    X-Coord=   4.075     Y-Coord= -0.1764E-16
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.4055E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.3245E-13
 S-eff =    1.240     Press.=  -0.7031E-14

 Node number     3    X-Coord=   10.19     Y-Coord= -0.1335E-16
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.1020E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.8163E-14
 S-eff =    1.240     Press.=   0.1269E-13

 Node number     4    X-Coord=   0.000     Y-Coord=   4.542    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.4650E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.3721E-13
 S-eff =    1.240     Press.=   0.2442E-14

 Node number     5    X-Coord=   5.603     Y-Coord=   5.552    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.1724E-14 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.1380E-12
 S-eff =    1.240     Press.=   0.1591E-14

 Node number     6    X-Coord=   10.19     Y-Coord=   5.047    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.4742E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.3795E-13
 S-eff =    1.240     Press.=   0.9474E-14

 Node number     7    X-Coord=   0.000     Y-Coord=   10.09    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.3002E-14 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.2402E-12
 S-eff =    1.240     Press.=   0.7587E-14

 Node number     8    X-Coord=   4.279     Y-Coord=   10.09    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =  -0.3860E-15 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =  -0.3089E-13
 S-eff =    1.240     Press.=   0.1018E-13

 Node number     9    X-Coord=   10.19     Y-Coord=   10.09    
 S-xx  =   0.7160     S-yy  =  -0.3531E-08 S-xy  =   0.3411E-14 S-zz  =  -0.7160    
 S-max =   0.7160     S-min =  -0.3531E-08 Angle =   0.2730E-12
 S-eff =    1.240     Press.=   0.6254E-14




 INCREMENT NUMBER    3
 =====================


 Total load factor ............... =    0.300000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    3                   TOTAL LOAD FACTOR =   0.300000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.351523E-01                 0.192259E-02
        2                     0.315398E-06                 0.172470E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   3 Load factor =   0.300000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.112500      -0.393651E-15
    3     0.281250      -0.905729E-15
    4      0.00000       0.635900E-01
    5     0.154688       0.777211E-01
    6     0.281250       0.706556E-01
    7      0.00000       0.141311    
    8     0.118125       0.141311    
    9     0.281250       0.141311    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -2.40601         0.689880E-08
    3      2.67334          0.00000    
    4     -5.34669          0.00000    
    6      5.34669          0.00000    
    7     -2.94068          0.00000    
    9      2.67334          0.00000    
       ---------------  ---------------
 Totals  -0.253131E-13     0.689880E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9379     Y-Coord=   1.010    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.1741E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.9458E-13
 S-eff =    1.826     Press.=   0.2813E-14

 Gauss point  2       X-Coord=   1.126     Y-Coord=   3.768    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.7935E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.4311E-13
 S-eff =    1.826     Press.=   0.2813E-14

 Gauss point  3       X-Coord=   3.500     Y-Coord=   1.133    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.1995E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.1084E-13
 S-eff =    1.826     Press.=  -0.2591E-14

 Gauss point  4       X-Coord=   4.203     Y-Coord=   4.230    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.1957E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.1064E-12
 S-eff =    1.826     Press.=  -0.2591E-14


 Element number    2

 Gauss point  1       X-Coord=   5.673     Y-Coord=   1.156    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.2605E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.1415E-13
 S-eff =    1.826     Press.=  -0.2591E-14

 Gauss point  2       X-Coord=   6.375     Y-Coord=   4.314    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.2759E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.1499E-12
 S-eff =    1.826     Press.=   0.7401E-16

 Gauss point  3       X-Coord=   9.047     Y-Coord=   1.094    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.1928E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.1047E-12
 S-eff =    1.826     Press.=   0.2813E-14

 Gauss point  4       X-Coord=   9.235     Y-Coord=   4.084    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.1339E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.7276E-13
 S-eff =    1.826     Press.=   0.7401E-16


 Element number    3

 Gauss point  1       X-Coord=   1.135     Y-Coord=   5.911    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.1277E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.6942E-14
 S-eff =    1.826     Press.=   0.2813E-14

 Gauss point  2       X-Coord=  0.9722     Y-Coord=   9.008    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.2198E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.1194E-12
 S-eff =    1.826     Press.=   0.2813E-14

 Gauss point  3       X-Coord=   4.237     Y-Coord=   6.373    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.2546E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.1383E-12
 S-eff =    1.826     Press.=   0.2813E-14

 Gauss point  4       X-Coord=   3.628     Y-Coord=   9.132    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.6227E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.3384E-13
 S-eff =    1.826     Press.=   0.2813E-14


 Element number    4

 Gauss point  1       X-Coord=   6.410     Y-Coord=   6.458    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.2317E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.1259E-12
 S-eff =    1.826     Press.=  -0.2591E-14

 Gauss point  2       X-Coord=   5.801     Y-Coord=   9.154    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.1779E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.9668E-13
 S-eff =    1.826     Press.=   0.7401E-16

 Gauss point  3       X-Coord=   9.244     Y-Coord=   6.227    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.7135E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.3877E-13
 S-eff =    1.826     Press.=   0.7401E-16

 Gauss point  4       X-Coord=   9.081     Y-Coord=   9.092    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.8091E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.4397E-13
 S-eff =    1.826     Press.=   0.7401E-16


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.3283E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.1784E-12
 S-eff =    1.826     Press.=   0.4737E-14

 Node number     2    X-Coord=   4.112     Y-Coord= -0.3937E-15
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.5482E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.2979E-13
 S-eff =    1.826     Press.=  -0.5477E-14

 Node number     3    X-Coord=   10.28     Y-Coord= -0.9057E-15
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.3767E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.2047E-12
 S-eff =    1.826     Press.=   0.6513E-14

 Node number     4    X-Coord=   0.000     Y-Coord=   4.564    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.8372E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.4549E-13
 S-eff =    1.826     Press.=   0.3775E-14

 Node number     5    X-Coord=   5.655     Y-Coord=   5.578    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.1046E-15 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.5686E-14
 S-eff =    1.826     Press.=  -0.1258E-14

 Node number     6    X-Coord=   10.28     Y-Coord=   5.071    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.1948E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.1058E-12
 S-eff =    1.826     Press.=    0.000    

 Node number     7    X-Coord=   0.000     Y-Coord=   10.14    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.4008E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.2178E-12
 S-eff =    1.826     Press.=   0.2813E-14

 Node number     8    X-Coord=   4.318     Y-Coord=   10.14    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =  -0.1721E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =  -0.9350E-13
 S-eff =    1.826     Press.=   0.2146E-14

 Node number     9    X-Coord=   10.28     Y-Coord=   10.14    
 S-xx  =    1.054     S-yy  =  -0.3355E-08 S-xy  =   0.1732E-14 S-zz  =   -1.054    
 S-max =    1.054     S-min =  -0.3355E-08 Angle =   0.9413E-13
 S-eff =    1.826     Press.=  -0.2961E-15




 INCREMENT NUMBER    4
 =====================


 Total load factor ............... =    0.400000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    4                   TOTAL LOAD FACTOR =   0.400000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.261123E-01                 0.187838E-02
        2                     0.229918E-06                 0.165369E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   4 Load factor =   0.400000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.150000      -0.259407E-15
    3     0.375000      -0.723196E-15
    4      0.00000       0.849213E-01
    5     0.206250       0.103793    
    6     0.375000       0.943570E-01
    7      0.00000       0.188714    
    8     0.157500       0.188714    
    9     0.375000       0.188714    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -3.16464         0.661476E-08
    3      3.51626          0.00000    
    4     -7.03253          0.00000    
    6      7.03253          0.00000    
    7     -3.86789          0.00000    
    9      3.51626          0.00000    
       ---------------  ---------------
 Totals  -0.230926E-13     0.661476E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9465     Y-Coord=   1.014    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.1619E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.6720E-13
 S-eff =    2.391     Press.=   0.9844E-14

 Gauss point  2       X-Coord=   1.136     Y-Coord=   3.786    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.7847E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.3257E-13
 S-eff =    2.391     Press.=   0.9844E-14

 Gauss point  3       X-Coord=   3.532     Y-Coord=   1.139    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.8027E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.3332E-13
 S-eff =    2.391     Press.=   0.4515E-14

 Gauss point  4       X-Coord=   4.241     Y-Coord=   4.250    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.1247E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.5175E-14
 S-eff =    2.391     Press.=   0.4515E-14


 Element number    2

 Gauss point  1       X-Coord=   5.725     Y-Coord=   1.161    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.4459E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.1851E-13
 S-eff =    2.391     Press.=   0.4515E-14

 Gauss point  2       X-Coord=   6.433     Y-Coord=   4.335    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.2819E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.1170E-12
 S-eff =    2.391     Press.=   0.7550E-14

 Gauss point  3       X-Coord=   9.129     Y-Coord=   1.099    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.1732E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.7190E-13
 S-eff =    2.391     Press.=   0.9844E-14

 Gauss point  4       X-Coord=   9.319     Y-Coord=   4.103    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.1708E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.7090E-13
 S-eff =    2.391     Press.=   0.7550E-14


 Element number    3

 Gauss point  1       X-Coord=   1.146     Y-Coord=   5.939    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.1756E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.7288E-14
 S-eff =    2.391     Press.=   0.9844E-14

 Gauss point  2       X-Coord=  0.9811     Y-Coord=   9.050    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.3987E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.1655E-12
 S-eff =    2.391     Press.=   0.9844E-14

 Gauss point  3       X-Coord=   4.276     Y-Coord=   6.403    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.3356E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.1393E-12
 S-eff =    2.391     Press.=   0.9844E-14

 Gauss point  4       X-Coord=   3.661     Y-Coord=   9.174    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.9467E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.3929E-13
 S-eff =    2.391     Press.=   0.9844E-14


 Element number    4

 Gauss point  1       X-Coord=   6.468     Y-Coord=   6.488    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.1916E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.7954E-13
 S-eff =    2.391     Press.=   0.4441E-14

 Gauss point  2       X-Coord=   5.854     Y-Coord=   9.197    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.1376E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.5712E-13
 S-eff =    2.391     Press.=   0.7550E-14

 Gauss point  3       X-Coord=   9.328     Y-Coord=   6.256    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.1470E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.6100E-13
 S-eff =    2.391     Press.=   0.7550E-14

 Gauss point  4       X-Coord=   9.164     Y-Coord=   9.135    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.1482E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.6149E-13
 S-eff =    2.391     Press.=   0.7550E-14


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.2244E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.9314E-13
 S-eff =    2.391     Press.=   0.1177E-13

 Node number     2    X-Coord=   4.150     Y-Coord= -0.2594E-15
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.9387E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.3896E-13
 S-eff =    2.391     Press.=   0.1628E-14

 Node number     3    X-Coord=   10.38     Y-Coord= -0.7232E-15
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.3486E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.1447E-12
 S-eff =    2.391     Press.=   0.1340E-13

 Node number     4    X-Coord=   0.000     Y-Coord=   4.585    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.6082E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.2524E-13
 S-eff =    2.391     Press.=   0.1088E-13

 Node number     5    X-Coord=   5.706     Y-Coord=   5.604    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.1200E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.4979E-13
 S-eff =    2.391     Press.=   0.5847E-14

 Node number     6    X-Coord=   10.38     Y-Coord=   5.094    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.2680E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.1112E-12
 S-eff =    2.391     Press.=   0.7476E-14

 Node number     7    X-Coord=   0.000     Y-Coord=   10.19    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.6605E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.2742E-12
 S-eff =    2.391     Press.=   0.9918E-14

 Node number     8    X-Coord=   4.357     Y-Coord=   10.19    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =  -0.3631E-15 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =  -0.1507E-13
 S-eff =    2.391     Press.=   0.9400E-14

 Node number     9    X-Coord=   10.38     Y-Coord=   10.19    
 S-xx  =    1.380     S-yy  =  -0.3188E-08 S-xy  =   0.2461E-14 S-zz  =   -1.380    
 S-max =    1.380     S-min =  -0.3188E-08 Angle =   0.1021E-12
 S-eff =    2.391     Press.=   0.7105E-14




 INCREMENT NUMBER    5
 =====================


 Total load factor ............... =    0.500000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    5                   TOTAL LOAD FACTOR =   0.500000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.206883E-01                 0.183528E-02
        2                     0.178767E-06                 0.158570E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   5 Load factor =   0.500000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.187500      -0.357934E-15
    3     0.468750      -0.105049E-14
    4      0.00000       0.106318    
    5     0.257813       0.129945    
    6     0.468750       0.118132    
    7      0.00000       0.236263    
    8     0.196875       0.236263    
    9     0.468750       0.236263    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -3.90279         0.634277E-08
    3      4.33643          0.00000    
    4     -8.67287          0.00000    
    6      8.67287          0.00000    
    7     -4.77008          0.00000    
    9      4.33643          0.00000    
       ---------------  ---------------
 Totals  -0.444089E-13     0.634277E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9551     Y-Coord=   1.019    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.1837E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.6210E-13
 S-eff =    2.935     Press.=   0.5033E-14

 Gauss point  2       X-Coord=   1.147     Y-Coord=   3.803    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.4032E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.1363E-13
 S-eff =    2.935     Press.=   0.5033E-14

 Gauss point  3       X-Coord=   3.564     Y-Coord=   1.144    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.1733E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.5860E-14
 S-eff =    2.935     Press.=  -0.2220E-15

 Gauss point  4       X-Coord=   4.279     Y-Coord=   4.270    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.9250E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.3128E-13
 S-eff =    2.935     Press.=  -0.2220E-15


 Element number    2

 Gauss point  1       X-Coord=   5.777     Y-Coord=   1.167    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.6252E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.2114E-13
 S-eff =    2.935     Press.=  -0.2220E-15

 Gauss point  2       X-Coord=   6.492     Y-Coord=   4.355    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.2240E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.7572E-13
 S-eff =    2.935     Press.=   0.1014E-13

 Gauss point  3       X-Coord=   9.211     Y-Coord=   1.104    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.2892E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.9779E-13
 S-eff =    2.935     Press.=   0.5033E-14

 Gauss point  4       X-Coord=   9.403     Y-Coord=   4.122    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.1223E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.4135E-14
 S-eff =    2.935     Press.=   0.1014E-13


 Element number    3

 Gauss point  1       X-Coord=   1.156     Y-Coord=   5.967    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.2425E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.8200E-14
 S-eff =    2.935     Press.=   0.1998E-14

 Gauss point  2       X-Coord=  0.9899     Y-Coord=   9.092    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.4487E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.1517E-12
 S-eff =    2.935     Press.=   0.2665E-14

 Gauss point  3       X-Coord=   4.314     Y-Coord=   6.433    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.4019E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.1359E-12
 S-eff =    2.935     Press.=   0.1998E-14

 Gauss point  4       X-Coord=   3.695     Y-Coord=   9.217    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.8378E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.2833E-13
 S-eff =    2.935     Press.=   0.2665E-14


 Element number    4

 Gauss point  1       X-Coord=   6.527     Y-Coord=   6.518    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.3845E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.1300E-12
 S-eff =    2.935     Press.=   0.1998E-14

 Gauss point  2       X-Coord=   5.907     Y-Coord=   9.240    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.4348E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.1470E-13
 S-eff =    2.935     Press.=   0.5033E-14

 Gauss point  3       X-Coord=   9.412     Y-Coord=   6.285    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.1014E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.3429E-13
 S-eff =    2.935     Press.=  -0.2220E-15

 Gauss point  4       X-Coord=   9.246     Y-Coord=   9.178    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.4178E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.1413E-12
 S-eff =    2.935     Press.=  -0.2220E-15


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.3015E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.1019E-12
 S-eff =    2.935     Press.=   0.7031E-14

 Node number     2    X-Coord=   4.187     Y-Coord= -0.3579E-15
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.3891E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.1316E-13
 S-eff =    2.935     Press.=  -0.4293E-14

 Node number     3    X-Coord=   10.47     Y-Coord= -0.1050E-14
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.4723E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.1597E-12
 S-eff =    2.935     Press.=   0.5773E-14

 Node number     4    X-Coord=   0.000     Y-Coord=   4.606    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.4471E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.1512E-13
 S-eff =    2.935     Press.=   0.4441E-14

 Node number     5    X-Coord=   5.758     Y-Coord=   5.630    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =  -0.2150E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =  -0.7271E-13
 S-eff =    2.935     Press.=   0.3775E-14

 Node number     6    X-Coord=   10.47     Y-Coord=   5.118    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.8991E-15 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.3040E-13
 S-eff =    2.935     Press.=   0.5403E-14

 Node number     7    X-Coord=   0.000     Y-Coord=   10.24    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.7536E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.2548E-12
 S-eff =    2.935     Press.=   0.2887E-14

 Node number     8    X-Coord=   4.397     Y-Coord=   10.24    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.1039E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.3512E-13
 S-eff =    2.935     Press.=   0.5847E-14

 Node number     9    X-Coord=   10.47     Y-Coord=   10.24    
 S-xx  =    1.695     S-yy  =  -0.3029E-08 S-xy  =   0.6557E-14 S-zz  =   -1.695    
 S-max =    1.695     S-min =  -0.3029E-08 Angle =   0.2217E-12
 S-eff =    2.935     Press.=  -0.2442E-14




 INCREMENT NUMBER    6
 =====================


 Total load factor ............... =    0.600000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    6                   TOTAL LOAD FACTOR =   0.600000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.170725E-01                 0.179326E-02
        2                     0.144776E-06                 0.152056E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   6 Load factor =   0.600000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.225000      -0.158645E-15
    3     0.562500      -0.585221E-15
    4      0.00000       0.127780    
    5     0.309375       0.156176    
    6     0.562500       0.141978    
    7      0.00000       0.283956    
    8     0.236250       0.283956    
    9     0.562500       0.283956    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -4.62115         0.608223E-08
    3      5.13461          0.00000    
    4     -10.2692          0.00000    
    6      10.2692          0.00000    
    7     -5.64807          0.00000    
    9      5.13461          0.00000    
       ---------------  ---------------
 Totals  -0.444089E-14     0.608223E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9636     Y-Coord=   1.024    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.8414E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.2414E-13
 S-eff =    3.459     Press.=   0.2072E-14

 Gauss point  2       X-Coord=   1.157     Y-Coord=   3.821    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.2553E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.7325E-14
 S-eff =    3.459     Press.=   0.6957E-14

 Gauss point  3       X-Coord=   3.596     Y-Coord=   1.149    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.6174E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.1771E-13
 S-eff =    3.459     Press.=   0.4515E-14

 Gauss point  4       X-Coord=   4.318     Y-Coord=   4.289    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.3404E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.9767E-14
 S-eff =    3.459     Press.=   0.4515E-14


 Element number    2

 Gauss point  1       X-Coord=   5.828     Y-Coord=   1.172    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.9260E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.2657E-13
 S-eff =    3.459     Press.=   0.2294E-14

 Gauss point  2       X-Coord=   6.550     Y-Coord=   4.375    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.5755E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.1651E-13
 S-eff =    3.459     Press.=   0.7401E-16

 Gauss point  3       X-Coord=   9.294     Y-Coord=   1.110    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.1955E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.5608E-13
 S-eff =    3.459     Press.=   0.6957E-14

 Gauss point  4       X-Coord=   9.487     Y-Coord=   4.141    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.5269E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.1512E-13
 S-eff =    3.459     Press.=   0.7401E-16


 Element number    3

 Gauss point  1       X-Coord=   1.166     Y-Coord=   5.994    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.4732E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.1358E-13
 S-eff =    3.459     Press.=  -0.2887E-14

 Gauss point  2       X-Coord=  0.9988     Y-Coord=   9.135    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.4114E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.1180E-12
 S-eff =    3.459     Press.=   0.2368E-14

 Gauss point  3       X-Coord=   4.353     Y-Coord=   6.463    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.3915E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.1123E-12
 S-eff =    3.459     Press.=  -0.2887E-14

 Gauss point  4       X-Coord=   3.728     Y-Coord=   9.260    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.2526E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.7246E-14
 S-eff =    3.459     Press.=   0.2368E-14


 Element number    4

 Gauss point  1       X-Coord=   6.585     Y-Coord=   6.548    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.4178E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.1199E-12
 S-eff =    3.459     Press.=   0.7179E-14

 Gauss point  2       X-Coord=   5.960     Y-Coord=   9.283    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.1150E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.3301E-13
 S-eff =    3.459     Press.=   0.2072E-14

 Gauss point  3       X-Coord=   9.497     Y-Coord=   6.314    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.9629E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.2762E-13
 S-eff =    3.459     Press.=   0.4515E-14

 Gauss point  4       X-Coord=   9.329     Y-Coord=   9.220    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.5015E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.1439E-12
 S-eff =    3.459     Press.=   0.4515E-14


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.1343E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.3854E-13
 S-eff =    3.459     Press.=  -0.1036E-14

 Node number     2    X-Coord=   4.225     Y-Coord= -0.1586E-15
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.9882E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.2835E-13
 S-eff =    3.459     Press.=   0.3331E-14

 Node number     3    X-Coord=   10.56     Y-Coord= -0.5852E-15
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.2844E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.8159E-13
 S-eff =    3.459     Press.=   0.1184E-13

 Node number     4    X-Coord=   0.000     Y-Coord=   4.628    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.6970E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.2000E-13
 S-eff =    3.459     Press.=   0.2813E-14

 Node number     5    X-Coord=   5.809     Y-Coord=   5.656    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =  -0.3201E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =  -0.9184E-13
 S-eff =    3.459     Press.=   0.2294E-14

 Node number     6    X-Coord=   10.56     Y-Coord=   5.142    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.5574E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.1599E-13
 S-eff =    3.459     Press.=  -0.7401E-16

 Node number     7    X-Coord=   0.000     Y-Coord=   10.28    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.7042E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.2020E-12
 S-eff =    3.459     Press.=   0.4293E-14

 Node number     8    X-Coord=   4.436     Y-Coord=   10.28    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.6750E-15 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.1937E-13
 S-eff =    3.459     Press.=   0.1406E-14

 Node number     9    X-Coord=   10.56     Y-Coord=   10.28    
 S-xx  =    1.997     S-yy  =  -0.2879E-08 S-xy  =   0.7742E-14 S-zz  =   -1.997    
 S-max =    1.997     S-min =  -0.2879E-08 Angle =   0.2221E-12
 S-eff =    3.459     Press.=   0.6143E-14




 INCREMENT NUMBER    7
 =====================


 Total load factor ............... =    0.700000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    7                   TOTAL LOAD FACTOR =   0.700000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.144901E-01                 0.175228E-02
        2                     0.120589E-06                 0.145817E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   7 Load factor =   0.700000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.262500      -0.963489E-16
    3     0.656250      -0.655945E-15
    4      0.00000       0.149306    
    5     0.360938       0.182485    
    6     0.656250       0.165896    
    7      0.00000       0.331792    
    8     0.275625       0.331792    
    9     0.656250       0.331792    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -5.32038         0.583269E-08
    3      5.91153          0.00000    
    4     -11.8231          0.00000    
    6      11.8231          0.00000    
    7     -6.50268          0.00000    
    9      5.91153          0.00000    
       ---------------  ---------------
 Totals  -0.408562E-13     0.583269E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9722     Y-Coord=   1.029    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.9378E-15 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.2348E-13
 S-eff =    3.964     Press.=   0.4885E-14

 Gauss point  2       X-Coord=   1.167     Y-Coord=   3.839    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.1754E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.4390E-13
 S-eff =    3.964     Press.=   0.1776E-14

 Gauss point  3       X-Coord=   3.628     Y-Coord=   1.155    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.5949E-16 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.1489E-14
 S-eff =    3.964     Press.=  -0.2368E-14

 Gauss point  4       X-Coord=   4.356     Y-Coord=   4.309    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.9657E-15 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.2418E-13
 S-eff =    3.964     Press.=  -0.2368E-14


 Element number    2

 Gauss point  1       X-Coord=   5.880     Y-Coord=   1.178    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.1099E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.2751E-13
 S-eff =    3.964     Press.=   0.2665E-14

 Gauss point  2       X-Coord=   6.608     Y-Coord=   4.396    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.1710E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.4281E-13
 S-eff =    3.964     Press.=   0.4737E-14

 Gauss point  3       X-Coord=   9.376     Y-Coord=   1.115    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.2218E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.5553E-13
 S-eff =    3.964     Press.=   0.1776E-14

 Gauss point  4       X-Coord=   9.571     Y-Coord=   4.160    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.7206E-15 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.1804E-13
 S-eff =    3.964     Press.=   0.4737E-14


 Element number    3

 Gauss point  1       X-Coord=   1.177     Y-Coord=   6.022    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.9290E-15 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.2326E-13
 S-eff =    3.964     Press.=   0.6809E-14

 Gauss point  2       X-Coord=   1.008     Y-Coord=   9.177    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.5495E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.1376E-12
 S-eff =    3.964     Press.=  -0.2368E-14

 Gauss point  3       X-Coord=   4.391     Y-Coord=   6.493    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.5596E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.1401E-12
 S-eff =    3.964     Press.=   0.6809E-14

 Gauss point  4       X-Coord=   3.761     Y-Coord=   9.303    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.1139E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.2852E-13
 S-eff =    3.964     Press.=  -0.2368E-14


 Element number    4

 Gauss point  1       X-Coord=   6.643     Y-Coord=   6.579    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.2249E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.5630E-13
 S-eff =    3.964     Press.=   0.1776E-14

 Gauss point  2       X-Coord=   6.013     Y-Coord=   9.326    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.1039E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.2602E-13
 S-eff =    3.964     Press.=   0.4885E-14

 Gauss point  3       X-Coord=   9.581     Y-Coord=   6.344    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.2215E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.5544E-13
 S-eff =    3.964     Press.=  -0.2368E-14

 Gauss point  4       X-Coord=   9.412     Y-Coord=   9.263    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.4492E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.1125E-12
 S-eff =    3.964     Press.=  -0.2368E-14


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.9728E-15 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.2435E-13
 S-eff =    3.964     Press.=   0.9178E-14

 Node number     2    X-Coord=   4.262     Y-Coord= -0.9635E-16
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.5469E-15 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.1369E-13
 S-eff =    3.964     Press.=  -0.1332E-14

 Node number     3    X-Coord=   10.66     Y-Coord= -0.6559E-15
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.3721E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.9316E-13
 S-eff =    3.964     Press.=   0.2961E-15

 Node number     4    X-Coord=   0.000     Y-Coord=   4.649    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.1930E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.4831E-13
 S-eff =    3.964     Press.=   0.5921E-14

 Node number     5    X-Coord=   5.861     Y-Coord=   5.682    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =  -0.3243E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =  -0.8118E-13
 S-eff =    3.964     Press.=   0.3701E-14

 Node number     6    X-Coord=   10.66     Y-Coord=   5.166    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.2301E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.5760E-13
 S-eff =    3.964     Press.=   0.1480E-14

 Node number     7    X-Coord=   0.000     Y-Coord=   10.33    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.9399E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.2353E-12
 S-eff =    3.964     Press.=  -0.5773E-14

 Node number     8    X-Coord=   4.476     Y-Coord=   10.33    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.1583E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.3963E-13
 S-eff =    3.964     Press.=   0.1628E-14

 Node number     9    X-Coord=   10.66     Y-Coord=   10.33    
 S-xx  =    2.289     S-yy  =  -0.2737E-08 S-xy  =   0.6455E-14 S-zz  =   -2.289    
 S-max =    2.289     S-min =  -0.2737E-08 Angle =   0.1616E-12
 S-eff =    3.964     Press.=  -0.5181E-14




 INCREMENT NUMBER    8
 =====================


 Total load factor ............... =    0.800000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    8                   TOTAL LOAD FACTOR =   0.800000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.125537E-01                 0.171233E-02
        2                     0.102529E-06                 0.139840E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   8 Load factor =   0.800000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.300000      -0.539737E-15
    3     0.750000      -0.126266E-14
    4      0.00000       0.170895    
    5     0.412500       0.208872    
    6     0.750000       0.189884    
    7      0.00000       0.379767    
    8     0.315000       0.379767    
    9     0.750000       0.379767    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -6.00109         0.559360E-08
    3      6.66788          0.00000    
    4     -13.3358          0.00000    
    6      13.3358          0.00000    
    7     -7.33467          0.00000    
    9      6.66788          0.00000    
       ---------------  ---------------
 Totals   0.150990E-13     0.559360E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9807     Y-Coord=   1.033    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.3016E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.6725E-13
 S-eff =    4.451     Press.=   0.1924E-14

 Gauss point  2       X-Coord=   1.177     Y-Coord=   3.857    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.1691E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.3770E-13
 S-eff =    4.451     Press.=   0.4293E-14

 Gauss point  3       X-Coord=   3.660     Y-Coord=   1.160    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.3873E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.8635E-14
 S-eff =    4.451     Press.=  -0.2961E-15

 Gauss point  4       X-Coord=   4.394     Y-Coord=   4.329    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.3014E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.6721E-14
 S-eff =    4.451     Press.=   0.1924E-14


 Element number    2

 Gauss point  1       X-Coord=   5.932     Y-Coord=   1.183    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.6461E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.1441E-13
 S-eff =    4.451     Press.=  -0.2961E-15

 Gauss point  2       X-Coord=   6.666     Y-Coord=   4.416    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.1699E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.3789E-13
 S-eff =    4.451     Press.=   0.4589E-14

 Gauss point  3       X-Coord=   9.459     Y-Coord=   1.120    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.2785E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.6210E-13
 S-eff =    4.451     Press.=   0.4293E-14

 Gauss point  4       X-Coord=   9.656     Y-Coord=   4.180    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.5854E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.1305E-13
 S-eff =    4.451     Press.=   0.4589E-14


 Element number    3

 Gauss point  1       X-Coord=   1.187     Y-Coord=   6.050    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.5190E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.1157E-13
 S-eff =    4.451     Press.=   0.2517E-14

 Gauss point  2       X-Coord=   1.017     Y-Coord=   9.220    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.6770E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.1510E-12
 S-eff =    4.451     Press.=  -0.4441E-15

 Gauss point  3       X-Coord=   4.430     Y-Coord=   6.523    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.5443E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.1214E-12
 S-eff =    4.451     Press.=   0.2517E-14

 Gauss point  4       X-Coord=   3.794     Y-Coord=   9.346    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.2270E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.5061E-13
 S-eff =    4.451     Press.=  -0.4441E-15


 Element number    4

 Gauss point  1       X-Coord=   6.702     Y-Coord=   6.609    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.3986E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.8888E-13
 S-eff =    4.451     Press.=   0.4293E-14

 Gauss point  2       X-Coord=   6.066     Y-Coord=   9.370    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.1543E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.3440E-13
 S-eff =    4.451     Press.=  -0.1480E-15

 Gauss point  3       X-Coord=   9.665     Y-Coord=   6.373    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.9646E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.2151E-13
 S-eff =    4.451     Press.=   0.1924E-14

 Gauss point  4       X-Coord=   9.495     Y-Coord=   9.306    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.5244E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.1169E-12
 S-eff =    4.451     Press.=   0.1924E-14


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.4548E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.1014E-12
 S-eff =    4.451     Press.=   0.2220E-14

 Node number     2    X-Coord=   4.300     Y-Coord= -0.5397E-15
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.1665E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.3713E-14
 S-eff =    4.451     Press.=  -0.3109E-14

 Node number     3    X-Coord=   10.75     Y-Coord= -0.1263E-14
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.4354E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.9708E-13
 S-eff =    4.451     Press.=   0.6661E-14

 Node number     4    X-Coord=   0.000     Y-Coord=   4.671    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.1589E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.3543E-13
 S-eff =    4.451     Press.=   0.4737E-14

 Node number     5    X-Coord=   5.913     Y-Coord=   5.709    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =  -0.3376E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =  -0.7527E-13
 S-eff =    4.451     Press.=   0.4885E-14

 Node number     6    X-Coord=   10.75     Y-Coord=   5.190    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.3709E-15 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.8271E-14
 S-eff =    4.451     Press.=   0.2368E-14

 Node number     7    X-Coord=   0.000     Y-Coord=   10.38    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.1103E-13 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.2459E-12
 S-eff =    4.451     Press.=  -0.1628E-14

 Node number     8    X-Coord=   4.515     Y-Coord=   10.38    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.2941E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.6557E-13
 S-eff =    4.451     Press.=  -0.2220E-14

 Node number     9    X-Coord=   10.75     Y-Coord=   10.38    
 S-xx  =    2.570     S-yy  =  -0.2602E-08 S-xy  =   0.7997E-14 S-zz  =   -2.570    
 S-max =    2.570     S-min =  -0.2602E-08 Angle =   0.1783E-12
 S-eff =    4.451     Press.=   0.3405E-14




 INCREMENT NUMBER    9
 =====================


 Total load factor ............... =    0.900000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER    9                   TOTAL LOAD FACTOR =   0.900000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.110480E-01                 0.167337E-02
        2                     0.885509E-07                 0.134116E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment   9 Load factor =   0.900000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.337500      -0.614572E-15
    3     0.843750      -0.147996E-14
    4      0.00000       0.192546    
    5     0.464063       0.235334    
    6     0.843750       0.213940    
    7      0.00000       0.427881    
    8     0.354375       0.427881    
    9     0.843750       0.427881    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -6.66390         0.536465E-08
    3      7.40434          0.00000    
    4     -14.8087          0.00000    
    6      14.8087          0.00000    
    7     -8.14477          0.00000    
    9      7.40434          0.00000    
       ---------------  ---------------
 Totals  -0.595080E-13     0.536465E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9893     Y-Coord=   1.038    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.2697E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.5441E-13
 S-eff =    4.919     Press.=  -0.1480E-15

 Gauss point  2       X-Coord=   1.188     Y-Coord=   3.875    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.1145E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.2311E-13
 S-eff =    4.919     Press.=  -0.2072E-14

 Gauss point  3       X-Coord=   3.692     Y-Coord=   1.165    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.7584E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.1530E-13
 S-eff =    4.919     Press.=   0.6513E-14

 Gauss point  4       X-Coord=   4.433     Y-Coord=   4.350    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.2341E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.4722E-13
 S-eff =    4.919     Press.=  -0.1480E-15


 Element number    2

 Gauss point  1       X-Coord=   5.984     Y-Coord=   1.189    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.1316E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.2655E-13
 S-eff =    4.919     Press.=  -0.4441E-15

 Gauss point  2       X-Coord=   6.724     Y-Coord=   4.436    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.2217E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.4472E-13
 S-eff =    4.919     Press.=   0.4145E-14

 Gauss point  3       X-Coord=   9.541     Y-Coord=   1.125    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.3319E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.6695E-13
 S-eff =    4.919     Press.=   0.4293E-14

 Gauss point  4       X-Coord=   9.740     Y-Coord=   4.199    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.3730E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.7524E-14
 S-eff =    4.919     Press.=   0.4145E-14


 Element number    3

 Gauss point  1       X-Coord=   1.197     Y-Coord=   6.078    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.5768E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.1163E-13
 S-eff =    4.919     Press.=   0.2072E-14

 Gauss point  2       X-Coord=   1.025     Y-Coord=   9.262    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.3780E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.7625E-13
 S-eff =    4.919     Press.=   0.6809E-14

 Gauss point  3       X-Coord=   4.469     Y-Coord=   6.553    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.2971E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.5994E-13
 S-eff =    4.919     Press.=  -0.2220E-14

 Gauss point  4       X-Coord=   3.827     Y-Coord=   9.390    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.4437E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.8951E-14
 S-eff =    4.919     Press.=   0.2072E-14


 Element number    4

 Gauss point  1       X-Coord=   6.760     Y-Coord=   6.640    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.3216E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.6489E-13
 S-eff =    4.919     Press.=   0.4441E-14

 Gauss point  2       X-Coord=   6.118     Y-Coord=   9.413    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.6174E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.1245E-13
 S-eff =    4.919     Press.=  -0.1480E-15

 Gauss point  3       X-Coord=   9.750     Y-Coord=   6.403    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.8415E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.1698E-13
 S-eff =    4.919     Press.=   0.1924E-14

 Gauss point  4       X-Coord=   9.578     Y-Coord=   9.349    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.2611E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.5267E-13
 S-eff =    4.919     Press.=   0.1924E-14


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.4913E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.9911E-13
 S-eff =    4.919     Press.=  -0.2813E-14

 Node number     2    X-Coord=   4.337     Y-Coord= -0.6146E-15
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.1469E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.2964E-13
 S-eff =    4.919     Press.=   0.3701E-14

 Node number     3    X-Coord=   10.84     Y-Coord= -0.1480E-14
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.5424E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.1094E-12
 S-eff =    4.919     Press.=   0.6661E-14

 Node number     4    X-Coord=   0.000     Y-Coord=   4.693    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.1473E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.2971E-13
 S-eff =    4.919     Press.=  -0.4441E-15

 Node number     5    X-Coord=   5.964     Y-Coord=   5.735    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.8342E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.1683E-13
 S-eff =    4.919     Press.=   0.1628E-14

 Node number     6    X-Coord=   10.84     Y-Coord=   5.214    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.1430E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.2886E-13
 S-eff =    4.919     Press.=   0.1776E-14

 Node number     7    X-Coord=   0.000     Y-Coord=   10.43    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.6145E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.1240E-12
 S-eff =    4.919     Press.=   0.1051E-13

 Node number     8    X-Coord=   4.554     Y-Coord=   10.43    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =  -0.1178E-15 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =  -0.2376E-14
 S-eff =    4.919     Press.=  -0.1036E-14

 Node number     9    X-Coord=   10.84     Y-Coord=   10.43    
 S-xx  =    2.840     S-yy  =  -0.2474E-08 S-xy  =   0.4329E-14 S-zz  =   -2.840    
 S-max =    2.840     S-min =  -0.2474E-08 Angle =   0.8733E-13
 S-eff =    4.919     Press.=   0.3109E-14




 INCREMENT NUMBER   10
 =====================


 Total load factor ............... =     1.00000    
 Incremental load factor ......... =    0.100000    
 Convergence tolerence ........... =    0.100000E-05
 Max. No. of iterations .......... =    10

 Output control flags for results
        ( 0 - No, 1 - Yes )
 Displacements...................... =   1
 Reactions.......................... =   1
 State variables at gauss points.... =   1
 State variables at nodes........... =   1
 Output to re-start file............ =   0




 INCREMENT NUMBER   10                   TOTAL LOAD FACTOR =    1.00000    
 --------------------------------------------------------------------------
                          relative residual             maximum residual
    iteration                 norm (%)                       norm       
 --------------------------------------------------------------------------
        1                     0.984372E-02                 0.163538E-02
        2                     0.774303E-07                 0.128632E-07
 --------------------------------------------------------------------------
                                  INCREMENTAL LOAD FACTOR =   0.100000    
 --------------------------------------------------------------------------


 Results for load increment  10 Load factor =    1.00000    
 ===========================================================

 Converged solution at iteration number =    2


 Displacement of structure from initial configuration
 ====================================================

 Node       X-Disp         Y-Disp
    1      0.00000        0.00000    
    2     0.375000       0.848011E-15
    3     0.937500       0.170364E-14
    4      0.00000       0.214259    
    5     0.515625       0.261872    
    6     0.937500       0.238065    
    7      0.00000       0.476130    
    8     0.393750       0.476130    
    9     0.937500       0.476130    


 Reactions
 =========

 Node      X-Force          Y-Force
    1     -7.30939         0.514527E-08
    3      8.12154          0.00000    
    4     -16.2431          0.00000    
    6      16.2431          0.00000    
    7     -8.93370          0.00000    
    9      8.12154          0.00000    
       ---------------  ---------------
 Totals   0.319744E-13     0.514527E-08


 Gauss point stresses and and other state variables
 ==================================================


 Element number    1

 Gauss point  1       X-Coord=  0.9978     Y-Coord=   1.043    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.3195E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.5904E-13
 S-eff =    5.371     Press.=   0.4589E-14

 Gauss point  2       X-Coord=   1.198     Y-Coord=   3.893    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.1947E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.3597E-13
 S-eff =    5.371     Press.=  -0.4589E-14

 Gauss point  3       X-Coord=   3.724     Y-Coord=   1.171    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.1495E-15 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.2763E-14
 S-eff =    5.371     Press.=   0.4589E-14

 Gauss point  4       X-Coord=   4.471     Y-Coord=   4.370    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.4218E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.7794E-13
 S-eff =    5.371     Press.=   0.6661E-14


 Element number    2

 Gauss point  1       X-Coord=   6.035     Y-Coord=   1.194    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.1519E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.2807E-13
 S-eff =    5.371     Press.=   0.5921E-14

 Gauss point  2       X-Coord=   6.782     Y-Coord=   4.457    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.1243E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.2297E-13
 S-eff =    5.371     Press.=   0.6069E-14

 Gauss point  3       X-Coord=   9.624     Y-Coord=   1.130    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.3400E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.6283E-13
 S-eff =    5.371     Press.=   0.6069E-14

 Gauss point  4       X-Coord=   9.824     Y-Coord=   4.218    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.1052E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.1943E-13
 S-eff =    5.371     Press.=   0.2368E-14


 Element number    3

 Gauss point  1       X-Coord=   1.208     Y-Coord=   6.106    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.8640E-15 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.1596E-13
 S-eff =    5.371     Press.=   0.2072E-14

 Gauss point  2       X-Coord=   1.034     Y-Coord=   9.305    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.2697E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.4983E-13
 S-eff =    5.371     Press.=  -0.2961E-15

 Gauss point  3       X-Coord=   4.507     Y-Coord=   6.584    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.3373E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.6233E-13
 S-eff =    5.371     Press.=   0.1480E-15

 Gauss point  4       X-Coord=   3.860     Y-Coord=   9.433    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.5317E-15 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.9825E-14
 S-eff =    5.371     Press.=  -0.2517E-14


 Element number    4

 Gauss point  1       X-Coord=   6.819     Y-Coord=   6.671    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.1075E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.1987E-13
 S-eff =    5.371     Press.=  -0.2220E-14

 Gauss point  2       X-Coord=   6.171     Y-Coord=   9.456    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.1112E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.2054E-13
 S-eff =    5.371     Press.=    0.000    

 Gauss point  3       X-Coord=   9.834     Y-Coord=   6.432    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.8929E-15 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.1650E-13
 S-eff =    5.371     Press.=  -0.2517E-14

 Gauss point  4       X-Coord=   9.660     Y-Coord=   9.393    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.5969E-15 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.1103E-13
 S-eff =    5.371     Press.=   0.6661E-14


 Averaged nodal stresses and other state variables for group number  1
 =====================================================================

 Node number     1    X-Coord=   0.000     Y-Coord=   0.000    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.6296E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.1163E-12
 S-eff =    5.371     Press.=   0.9770E-14

 Node number     2    X-Coord=   4.375     Y-Coord=  0.8480E-15
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.1214E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.2242E-13
 S-eff =    5.371     Press.=   0.4145E-14

 Node number     3    X-Coord=   10.94     Y-Coord=  0.1704E-14
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.4893E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.9041E-13
 S-eff =    5.371     Press.=   0.7846E-14

 Node number     4    X-Coord=   0.000     Y-Coord=   4.714    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.2152E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.3976E-13
 S-eff =    5.371     Press.=  -0.5033E-14

 Node number     5    X-Coord=   6.016     Y-Coord=   5.762    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.4320E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.7982E-13
 S-eff =    5.371     Press.=   0.4885E-14

 Node number     6    X-Coord=   10.94     Y-Coord=   5.238    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.1422E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.2627E-13
 S-eff =    5.371     Press.=  -0.3849E-14

 Node number     7    X-Coord=   0.000     Y-Coord=   10.48    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.4746E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.8769E-13
 S-eff =    5.371     Press.=  -0.2961E-15

 Node number     8    X-Coord=   4.594     Y-Coord=   10.48    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =  -0.2505E-15 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =  -0.4628E-14
 S-eff =    5.371     Press.=  -0.3405E-14

 Node number     9    X-Coord=   10.94     Y-Coord=   10.48    
 S-xx  =    3.101     S-yy  =  -0.2352E-08 S-xy  =   0.1079E-14 S-zz  =   -3.101    
 S-max =    3.101     S-min =  -0.2352E-08 Angle =   0.1994E-13
 S-eff =    5.371     Press.=   0.1347E-13
