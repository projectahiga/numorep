#!/bin/bash

. ../../../scripts/run_support_bash

NAME=simogd
DRIVER=$(nmrFindInPath feap feap/7.4)

if [ $? -ne 0 ]; then
  echo ERROR: $DRIVER
  exit 1
fi

nmrSetupLog

echo "RUNNING $DRIVER"

$DRIVER -iI$NAME
STATUS=$?

nmrAddLog 

nmrPrintRunMessage
