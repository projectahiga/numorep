#pragma once

#include <exception>
#include <string>

namespace sof {

class SofException: public std::exception {
  public:
    SofException(void)  : message("Unspecified exception for"
                                " sofem program") {}
    explicit SofException(const char *msg) : message(msg) {}
    explicit SofException(const std::string & msg) : message(msg) {}
    virtual ~SofException() throw () {}
    virtual const char* what() const throw() {
       return message.c_str(); 
    }
  protected:
    std::string message;
};

class ManagerError : public SofException {
public:
  ManagerError(void)  : SofException("Unspecified exception for"
                                " sofem Manager") {}
  ManagerError(const std::string &msg) : SofException(msg) {} 
};

class ParseError : public SofException {
public:
  ParseError() : SofException("Unspecified parse error") {}
  ParseError(const std::string &explanation, const std::string &culprit) {
    message = explanation + " : " + culprit; 
  }
};

} // namespace sof
