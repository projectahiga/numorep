// Copyright (c) 2018 Roman Putanowicz
//! @brief Implementation of boundary data handling.

#include "bc.h"

namespace sof {

const std::vector<const char*> BCData::validNames = {
  "displacement",
  "fixed",
  "free",
  "pressure",
  "sliding",
  "traction",
  "ramp_traction"
};

BCData::BCData(const std::string &name, int size, const double *v) : type_name_(name) {
  setup_(name, size, v);
}

BCData::BCData(const std::string &name, const std::string &control, 
               double tresholdMin, double tresholdMax, 
               double x, double y, int size, const double *vA,
               const double *vB) : type_name_(name) {
  setup_(name, size, vA, vB);
  treshold_[0] = tresholdMin;
  treshold_[1] = tresholdMax;
  refPoint_[0] = x;
  refPoint_[1] = y;
}

void BCData::setup_value_(int size, const double *v, std::vector<double> &mval) {
  mval.resize(2, 0.0);
  if (size > 0) {
    mval.resize(size);
    if (v != nullptr) {
      for(int i=0; i<size; ++i) mval[i] = v[i];
    }
  }
}
void BCData::setup_type_(const std::string &name) {
  for (int i=0; i<validNames.size(); ++i) {
    if (name == std::string(validNames.at(i))) {
      type_ = static_cast<enum BCData::Type>(i);
      break;
    }
  }
}
void BCData::setup_(const std::string &name, int size, const double *v) {
  setup_type_(name);
  setup_value_(size, v, value_);
}
void BCData::setup_(const std::string &name, int size, const double *vA,
                    const double *vB) {
  setup_type_(name);
  setup_value_(size, vA, vA_);
  setup_value_(size, vB, vB_);
}

const bgeot::base_node& BCData::get_ref_point() const {
  return refPoint_;
}

void BCData::set_ref_point(const bgeot::base_node &refPoint) {
  refPoint_ = refPoint;
}


enum BCData::Type BCData::type() const {
  return type_;
}

const std::string &BCData::type_name() const {
  return type_name_;
}

const std::vector<double> &BCData::value() const {
  return value_;
}

const std::vector<double> &BCData::value(const std::vector<double> &driver) const {
  double t = 0.0;
  if (control_ == "X") {
    t = driver[0];
  } else if (control_ == "Y") {
    t = driver[1];
  } else if (control_ == "Magnitude") {
    t = sqrt(driver[0]*driver[0] + driver[1]*driver[1]);
  }
  double xi = 0.0;
  if (t > treshold_[1]) xi = 1.0;
  if (t < treshold_[0]) xi = 0.0;

  xi = (t-treshold_[0])/(treshold_[1] - treshold_[0]);

  value_.resize(vA_.size(),0.0);
  for (int i=0; i<vA_.size(); i++) {
     value_[i] = vA_[i]*(1-xi) + vB_[i]*xi;
  }
  return value_;
}

} // namespace sof
