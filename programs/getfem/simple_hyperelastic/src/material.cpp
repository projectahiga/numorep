// Copyright (c) 2018 Roman Putanowicz
//! @brief Implementation of SimpleModel class.

#include "manager.h"
#include "material.h"

#include <easylogging++.h>

#include <vector>
#include <array>
#include <stdexcept>
#include <sstream>

namespace sof {

const std::vector<std::string> Material::validLaws_ = {"Neo_Hookean", "Linear_Elasticity"};

Material::Material(const Manager &manager) {
  setup_(manager);
}

bool Material::is_valid_law(const std::string &name) {
  bool status = true;
  if (std::find(std::begin(validLaws_), std::end(validLaws_), name) == std::end(validLaws_)) {
    status=false;
  }
  return status;
}

void Material::setup_law_name_and_compressibility_(const Manager &manager) {
  isCompressible_ = manager.is_material_compressible();
  lawName_ = manager.material_law();
  if (is_valid_law(lawName_)) {
    if (lawName_ != std::string("Linear_Elasticity")) {
      if (isCompressible_) {
        lawName_ = std::string("Compressible_") + lawName_;
      } else {
        lawName_ = std::string("Incompressible_") + lawName_;
      }
    }
  } else {
    std::stringstream oss;
    oss << "Invalid law name : " << lawName_;
    throw oss.str();
  }
}

void Material::setup_from_mu_lambda_(const Manager &manager) {
  const double mu = manager.mu();
  const double lambda = manager.lambda();
  const double E = mu*(3*lambda + 2*mu)/(lambda+mu);
  const double nu = E/(2*(mu+lambda));
  constants_["mu"] = mu;
  constants_["lambda"] = lambda;
  constants_["E"] = E;
  constants_["nu"] = nu;
}

void Material::setup_from_E_nu_(const Manager &manager) {
  const double E = manager.E();
  double nu = manager.nu();
  if ( nu > 0.4999) {
    std::cerr << "Warning: Incompressibility limit reached. Setting nu to limitig value: " << nuLimit_;
    nu = nuLimit_;
  }
  const double mu = E/(2*(1+nu));
  const double lambda = E*nu/((1+nu)*(1-2*nu));

  constants_["mu"] = mu;
  constants_["lambda"] = lambda;
  constants_["E"] = E;
  constants_["nu"] = nu;
}

void Material::setup_material_law_params_(const Manager &manager) {
  if (is_compressible()) {
    params_.resize(2, 0.0);
    params_[0] = constants_["mu"]/2.0;
    params_[1] = constants_["lambda"]/2.0;
  } else {
    params_.resize(1, 0.0);
    params_[0] = constants_["mu"]/2.0;
  }
}

void Material::setup_(const Manager &manager) {
  if (manager.has_mu_lambda()) {
    setup_from_mu_lambda_(manager);
  } else if (manager.has_E_nu()) {
    setup_from_E_nu_(manager);
  } else {
    throw std::logic_error("Error: Cannot setup  do to missing (mu, lambda) or (E,nu)");
  }
  setup_law_name_and_compressibility_(manager);
  setup_material_law_params_(manager);
}

double Material::get_constant_(const std::string &name) const {
  auto iter = constants_.find(name);
  if (iter != constants_.end()) {
    return iter->second;
  } else {
    std::stringstream oss;
    oss << "ERROR: internal error, material constant '" << name << "' not set in material class";
    throw std::logic_error(oss.str());
  }
}


} // namespace sof
