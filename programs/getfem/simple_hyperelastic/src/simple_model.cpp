// Copyright (c) 2018 Roman Putanowicz
//! @brief Implementation of SimpleModel class.

#include "bc.h"
#include "geometry.h"
#include "global_function.h"
#include "manager.h"
#include "parsing.h"
#include "simple_model.h"
#include "dof_algorithms.h"
#include "vtk2getfem.h"

#include <easylogging++.h>

#include <armadillo>

#include <getfem/getfem_deformable_mesh.h>
#include <getfem/getfem_export.h>
#include <getfem/getfem_interpolation.h>
#include <getfem/getfem_model_solvers.h>
#include <getfem/getfem_models.h>
#include <getfem/getfem_nonlinear_elasticity.h>
#include <getfem/getfem_regular_meshes.h>
#include <getfem/getfem_generic_assembly.h>
#include <gmm/gmm.h>

#include <vector>
#include <algorithm>
#include <array>
#include <stdexcept>
#include <sstream>

/* some GetFEM++ types that we will be using */
using bgeot::base_small_vector; /* special class for small (dim<16) vectors  */
using bgeot::base_node;  /* geometrical nodes(derived from base_small_vector)*/
using bgeot::base_vector;
using bgeot::scalar_type; /* = double */
using bgeot::size_type;   /* = unsigned long */
using bgeot::dim_type;
using bgeot::base_matrix; /* small dense matrix. */

/* definition of some matrix/vector types. These ones are built
 * using the predefined types in Gmm++
 */
typedef getfem::modeling_standard_sparse_vector sparse_vector;
typedef getfem::modeling_standard_sparse_matrix sparse_matrix;
typedef getfem::modeling_standard_plain_vector  plain_vector;

namespace sof {

SimpleModel::SimpleModel(const Manager &manager) : manager_(manager), mim_(mesh_), mfu_(mesh_), mfp_(mesh_), mfmat_(mesh_), mfrhs_(mesh_), mfcoef_(mesh_), material_(manager), mfsf_(mesh_), mfsd_(mesh_) {
  LOG(INFO) << "Creating simple model" << std::endl;
  configure_();
}

void SimpleModel::configure_() {
  LOG(INFO) << "Configuring model from manager" << std::endl;
  build_parametric_mesh_();
  set_boundary_regions_();
  build_mesh_();
  set_fem_and_im_();
  set_data_();
  set_builtInFields_();
}

void SimpleModel::set_data_() {
  add_initialized_scalar_data("mu", material_.mu());
  add_initialized_scalar_data("lambda", material_.lambda());
  add_initialized_scalar_data("Eyoung", material_.E());
  add_initialized_scalar_data("nu", material_.nu());
}

void SimpleModel::set_builtInFields_() {
  add_macro("F", "Id(meshdim)+Grad_u");
  add_macro("E", "0.5*(F'*F-Id(meshdim))");
  add_macro("b", "F*F'");
  add_macro("J", "Det(F)");
  add_macro("S", "lambda*Trace(E)*Id(meshdim)+2*mu*E");
  builtInFields_.emplace(
    std::make_pair(std::string("F"), ComputedField(mfmat_, "F", "Deformation gradient"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("u"), ComputedField(mfu_, "u", "Displacement field"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("TestU"), ComputedField(mfsf_, "Test_u", "Shape fun"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("GradTestU"), ComputedField(mfsd_, "Grad_Test_u", "Shape fun"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("p"), ComputedField(mfp_, "p", "Pressure field"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("C"), ComputedField(mfmat_, "F'*F", "Right Cauchy-Green deformation tensor"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("E"), ComputedField(mfmat_, "E", "Green-Lagrange strain"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("b"), ComputedField(mfmat_, "b", "Left Cauchy-Green deformation tensor"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("e"), ComputedField(mfmat_, "0.5*(Id(meshdim)-Inv(F*F'))", "Euler-Almansi Strain tensor"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("S"), ComputedField(mfmat_, "S", "2nd P-K stress"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("J"), ComputedField(mfrhs_, "J", "Jacobian of deformation gradient"))
  );
  builtInFields_.emplace(
    std::make_pair(std::string("sigma"), ComputedField(mfmat_, "1/J*F*S*F'", "Jacobian of deformation gradient"))
  );
}

void SimpleModel::write_u_dofs(std::ostream &out, bool writeCoords) const {
  std::vector<size_type> dofs;
  for (auto i : dofs) {
    out << i << std::endl;
  }
}

void SimpleModel::sample(const std::string &fieldName) {
  getfem::mesh_trans_inv pts(mesh_);
  for (size_t i=0; i<manager_.probe_points_count(); ++i) {
    bgeot::base_node pt;
    manager_.get_probe_point(i, pt);
    if (manager_.are_parametric_probes()) {
      transform_probe_point_(pt);
    }
    pts.add_point_with_id(pt, i);
  }
  //getfem::ga_workspace ga(*this);
  auto Np = manager_.probe_points_count();
  auto fIter = builtInFields_.find(fieldName);
  if (fIter != builtInFields_.end()) {
    std::vector<double> result(Np*fIter->second.fem.get_qdim());
    getfem::ga_interpolation_mti(*this, fIter->second.expr, pts, result);
    std::cout << "=========================================" << std::endl;
    std::cout << "Sampling field " << fieldName << ", #samples : " << Np << std::endl;
    std::cout << "-------------------" << std::endl;
    auto qd = fIter->second.fem.get_qdims();
    auto Nr = qd[0];
    auto Nc = qd.size() > 1 ? qd[1] : 1;
    auto Nv = Nc*Nr;
    for (size_t i=0; i<Np; ++i) {
       arma::mat M(&result[0]+i*Nv,Nr,Nc);
       print_probe_point_(std::cout, i);
       std::cout << M << std::endl;
       std::cout << "-------------------" << std::endl;
    }
  }
}

void SimpleModel::listranges(std::ostream &out, const std::vector<std::string> &rangesToShow) const {
  out << "Ranges of variables: " << std::endl;
  for (auto &&text : rangesToShow) {
    auto niPair = parseNameIndex(text);
    auto &dofs = real_variable(niPair.first);
    auto &fem = mesh_fem_of_variable(niPair.first);
    int component = niPair.second;
    auto minmax = std::make_pair(dofs.begin(), dofs.begin());
    if (component >= 0) {
      minmax = dof_minmax(dofs, component, fem.get_qdim());
    } else {
      minmax = std::minmax_element(dofs.begin(), dofs.end());
    }
    out << text << " : " << *(minmax.first) << " " << *(minmax.second) 
        << std::endl;
  }
}

void SimpleModel::listvariables(std::ostream &out) const {
  for (auto && name : variable_names()) {
    out << name << std::endl;  
  }
}

void SimpleModel::listdofs(std::ostream &out) const {
  for (auto && name : variable_names()) {
    auto &I = interval_of_variable(name);
    out <<  "Variable " << name << " dofs: " << I << std::endl;  
  }
}

void SimpleModel::print_probe_point_(std::ostream &out, size_t index) const {
  bgeot::base_node pt;
  manager_.get_probe_point(index, pt);
  out << "Point " <<  index << " : " << pt;
  if (manager_.are_parametric_probes()) {
    transform_probe_point_(pt);
    out << " physical coords : " << pt;
  }
  out << std::endl;
}

void SimpleModel::compute_displacement_() {
  switch(manager_.displacement_source()) {
    case USourceType::EXPRESSION :
      displacement_from_expression_();
      break;
    case USourceType::SIMPLE_SHEAR :
      displacement_simple_shear_();
      break;
    default:
      std::stringstream oss;
      oss << "Unhandled displacement source";
      throw std::logic_error(oss.str());
  }
}

void SimpleModel::displacement_from_expression_() {
  LOG(INFO) << "Expression displacement: " <<  manager_.u_field_expr() << std::endl;
  GlobalVectorFunction fun("u", manager_.u_field_expr());
  if (fun.dim() != mfu_.get_qdim()) {
    std::stringstream oss;
    oss << "Invalid dim of u expression. Expected " << mfu_.get_qdim()
        << " got " << fun.dim();
    throw std::logic_error(oss.str());
  }
  getfem::interpolation_function(mfu_, this->set_real_variable("u"), fun);
}

void SimpleModel::displacement_simple_shear_() {
  LOG(INFO) << "Simple shear displacement" << std::endl;
  SimpleShearDisplacement fun("u", manager_.shear_k());
  if (fun.dim() != mfu_.get_qdim()) {
    std::stringstream oss;
    oss << "Invalid dim of u expression. Expected " << mfu_.get_qdim()
        << " got " << fun.dim();
    throw std::logic_error(oss.str());
  }
  getfem::interpolation_function(mfu_, this->set_real_variable("u"), fun);
}

void SimpleModel::analyze() {
  add_fem_variable("u", mfu_);

  if (manager_.haveUFile) {
    v2g::readModel(mesh_, *this, manager_.ufieldFile);
  } else {
    compute_displacement_();
  }

  getfem::vtk_export exporter("analysis.vtk", true);
  exporter.exporting(mesh_);
  exporter.exporting(mfu_);
  exporter.write_point_data(mfu_, this->real_variable("u"), "u");
  for (auto &&fieldName : manager_.sampleFields()) {
    sample(fieldName);
  }
}

void SimpleModel::write_rhs_full_() const {
  const auto &rhs = real_rhs();
  const auto ndof = rhs.size();
  for (int i=0; i<ndof; ++i) {
    std::cout << i << " " << rhs[i] << std::endl;
  }
}

void SimpleModel::write_rhs_compact_() const {
  for (auto v : real_rhs()) {
    std::cout << " " << v;
  }
  std::cout << std::endl;
}

void SimpleModel::write_rhs_() const {
  manager_.reporter().section_start("Right Hand Side");
  switch (manager_.rhs_type()) {
    case Manager::RHSType::COMPACT:
      write_rhs_compact_();
      break;
    case Manager::RHSType::FULL:
      write_rhs_full_();
      break;
    default:
      throw std::logic_error("Invalid RHSType");
  }
  manager_.reporter().section_end();
}

void SimpleModel::write_mesh() {
  getfem::vtk_export exporter("out.vtk", true);
  exporter.exporting(mesh_);
  exporter.write_mesh();
  mesh_.write_to_file("out.msh");
}

void SimpleModel::write_solution() {
  getfem::vtk_export exporter("solution.vtk", true);
  exporter.exporting(mesh_);
  exporter.exporting(mfu_);
  exporter.write_point_data(mfu_, this->real_variable("u"), "u");
  auto problem = manager_.problem_type();
  if (variable_exists("p")) {
    exporter.write_point_data(mfp_, this->real_variable("p"), "p");
  }
}

void SimpleModel::transform_probe_point_(bgeot::base_node &pt) const {
  auto mtype = manager_.mesh_type();
  if (mtype == std::string("cooks_membrane")) {
    double  W = manager_.cooks_membrane("W");
    double  H = manager_.cooks_membrane("H");
    double HL = manager_.cooks_membrane("HL");
    double TD = manager_.cooks_membrane("TD");
    ToCooksMembraneDisplacement uField(W, H, HL, TD);
    auto trpt = uField(pt);
    for (size_t i=0; i<pt.size(); ++i) {
      pt[i] +=trpt[i];
    }
  }
}

void SimpleModel::build_mesh_() {
  auto mtype = manager_.mesh_type();
  if (mtype == std::string("simple")) {
    build_simple_patch_mesh_();
  } else if (mtype == std::string("regular")) {
    build_regular_mesh_();
  } else if (mtype == std::string("cooks_membrane")) {
    build_cooks_membrane_mesh_();
  } else {
    throw std::logic_error("Unknow mesh type");
  }
}

void SimpleModel::build_parametric_mesh_() {
  mesh_.clear();
  pgt_ = bgeot::parallelepiped_geotrans(2,1);
  std::vector<getfem::size_type> nsubdiv(2);
  nsubdiv[0] = manager_.mesh_resolution(X_DIR);
  nsubdiv[1] = manager_.mesh_resolution(Y_DIR);
  getfem::regular_unit_mesh(mesh_, nsubdiv, pgt_);
}

void SimpleModel::build_regular_mesh_() {
  const double Lx = manager_.length(X_DIR);
  const double Ly = manager_.length(Y_DIR);
  base_matrix M(N_, N_);
  M.fill(0.0);
  M(X_DIR,X_DIR) = manager_.length(X_DIR);
  M(Y_DIR,Y_DIR) = manager_.length(Y_DIR);
  mesh_.transformation(M);
}

void SimpleModel::build_cooks_membrane_mesh_() {
  getfem::mesh_fem mfdeform(mesh_);
  mfdeform.set_classical_finite_element(1);
  mfdeform.set_qdim(N_);
  double  W = manager_.cooks_membrane("W");
  double  H = manager_.cooks_membrane("H");
  double HL = manager_.cooks_membrane("HL");
  double TD = manager_.cooks_membrane("TD");
  ToCooksMembraneDisplacement uField(W, H, HL, TD);
  plain_vector u(mfdeform.nb_dof(),0.0);
  getfem::interpolation_function(mfdeform, u, uField);
  getfem::temporary_mesh_deformator<plain_vector> deformator(mfdeform, u, true, false);
}

void SimpleModel::build_simple_patch_mesh_() {
  mesh_.clear();
  const std::vector<std::array<double, 3>> coords = {
                    {  0.00, 0.00, 0.00},
                    {  0.40, 0.00, 0.00},
                    {  1.00, 0.00, 0.00},
                    {  0.00, 0.45, 0.00},
                    {  0.55, 0.55, 0.00},
                    {  1.00, 0.50, 0.00},
                    {  0.00, 1.00, 0.00},
                    {  0.42, 1.00, 0.00},
                    {  1.00, 1.00, 0.00}};
  const std::vector<std::array<bgeot::size_type, 4>> elements = {{0, 1, 3, 4},
                                                           {1, 2, 4, 5},
                                                           {3, 4, 6, 7},
                                                           {4, 5, 7, 8}};
  const double Lx = manager_.length(X_DIR);
  const double Ly = manager_.length(Y_DIR);
  std::vector<bgeot::size_type> ind(coords.size());
  for (int i=0; i<coords.size(); ++i) {
    ind[i] = mesh_.add_point(bgeot::base_node(coords[i][0]*Lx, coords[i][1]*Ly));
  }
  for (int i=0; i<elements.size(); ++i) {
    mesh_.add_convex(pgt_, elements[i].begin());
  }
}

void SimpleModel::set_boundary_regions_() {
  getfem::mesh_region border_faces;
  getfem::outer_faces_of_mesh(mesh_, border_faces);
  std::cout << "Mesh dimension " << mesh_.dim() << std::endl;
  std::cout << "Number of boundary faces " << border_faces.size() << std::endl;
  for (getfem::mr_visitor it(border_faces); !it.finished(); ++it) {
    assert(it.is_face());
    base_node un = mesh_.normal_of_face_of_convex(it.cv(), it.f());
    un /= gmm::vect_norm2(un);
    for (auto &side : squareSides()) {
      if (onSide(side, un)) {
        mesh_.region(side).add(it.cv(),it.f());
        break;
      }
    }
  }
}

void SimpleModel::set_fem_and_im_() {
  mfu_.set_qdim(N_);
  mfrhs_.set_qdim(1);
  mfmat_.set_qdim(N_, N_);

  getfem::pintegration_method ppi = getfem::int_method_descriptor(manager_.integration_type());

  mim_.set_integration_method(ppi);
  auto u_pfem =  getfem::fem_descriptor(manager_.fem_type("u"));
  mfu_.set_finite_element(u_pfem);
  mfp_.set_finite_element(getfem::fem_descriptor(manager_.fem_type("p")));
  mfrhs_.set_finite_element(getfem::fem_descriptor(manager_.fem_type("data")));
  mfmat_.set_finite_element(getfem::fem_descriptor(manager_.fem_type("data")));
  mfcoef_.set_finite_element(mesh_.convex_index(), getfem::classical_fem(pgt_, 0));
  mfsf_.set_qdim(u_pfem->nb_base(0)*N_,N_);
  mfsd_.set_qdim(u_pfem->nb_base(0)*N_,N_*N_);
}

void SimpleModel::set_Neumann_data_() {
  neumannData_.resize(mfmat_.nb_dof(), 0.0);
  for (auto &side : squareSides()) {
    auto dofs = mfmat_.basic_dof_on_region(mesh_.region(side));
    auto bcData = manager_.bc_data(sideNames().at(side));
    auto value = bcData.value();
    std::vector<double> vals = {value[1], -value[0], value[0], value[1]};
    switch (bcData.type()) {
      case BCData::PRESSURE :
      case BCData::TRACTION:
        for (dal::bv_visitor i(dofs); !i.finished(); ++i) {
          neumannData_[i] = vals[mfmat_.basic_dof_qdim(i)];
          neumannDofs_.add(i);
        }
        break;
      case BCData::DISPLACEMENT :
      case BCData::FIXED :
      case BCData::FREE:
      case BCData::SLIDING:
        break;
      default:
        throw std::logic_error("Unsupported boundary condition type");
    }
  }
  add_initialized_fem_data("NeumannData", mfmat_, neumannData_);
  for (auto &side : squareSides()) {
    auto bcData = manager_.bc_data(sideNames().at(side));
    switch (bcData.type()) {
      case BCData::PRESSURE:
      case BCData::TRACTION:
       getfem::add_normal_source_term_brick(*this, mim_, "u", "NeumannData", side);
       break;
      case BCData::DISPLACEMENT :
      case BCData::FIXED :
      case BCData::FREE:
      case BCData::SLIDING:
        break;
      default:
        throw std::logic_error("Unsupported boundary condition type");
    }
  }
}

void SimpleModel::update_Dirichlet_data_(double factor) {
  update_data_(factor, "DirichletData", dirichletData_, dirichletDofs_);
}

void SimpleModel::update_Neumann_data_(double factor) {
  update_data_(factor, "NeumannData", neumannData_, neumannDofs_);
}

void SimpleModel::update_data_(double factor, const std::string &dataName,
                               const std::vector<double> &dataSource,
                               const dal::bit_vector &dofs) {
  auto &target = set_real_variable(dataName);
  for (dal::bv_visitor i(dofs); !i.finished(); ++i) {
    target[i] = dataSource[i]*factor;
  }
}

void SimpleModel::set_Dirichlet_data_() {
  dirichletData_.resize(mfrhs_.nb_dof()*N_, 0.0);
  for (auto &side : squareSides()) {
    auto dofs = mfu_.basic_dof_on_region(mesh_.region(side));
    auto bcData = manager_.bc_data(sideNames().at(side));
    auto value = bcData.value();
    switch (bcData.type()) {
      case BCData::DISPLACEMENT :
      case BCData::FIXED :
        for (dal::bv_visitor i(dofs); !i.finished(); ++i) {
          dirichletData_[i] = value.at(mfu_.basic_dof_qdim(i));
          dirichletDofs_.add(i);
        }
        break;
      case BCData::FREE:
      case BCData::PRESSURE:
      case BCData::SLIDING:
      case BCData::TRACTION:
        break;
      default:
        throw std::logic_error("Unsupported boundary condition type");
    }
  }
  add_initialized_fem_data("DirichletData", mfrhs_, dirichletData_);
  for (auto &side : squareSides()) {
    auto bcData = manager_.bc_data(sideNames().at(side));
    switch (bcData.type()) {
      case BCData::DISPLACEMENT :
      case BCData::FIXED :
        getfem::add_Dirichlet_condition_with_multipliers(*this, mim_,
                                   "u", mfu_, side, "DirichletData");
        break;
      case BCData::FREE:
      case BCData::PRESSURE:
        break;
      case BCData::SLIDING:
        getfem::add_normal_Dirichlet_condition_with_multipliers(*this, mim_,
                                   "u", mfrhs_, side);
        break;
      case BCData::TRACTION:
        break;
      default:
        throw std::logic_error("Unsupported boundary condition type");
    }
  }
}

void SimpleModel::solve() {
  auto problem = manager_.problem_type();
  if (problem == std::string("displacement")) {
    solve_linear_elasticity_();
  } else if (problem == std::string("mixed")) {
    solve_linear_elasticity_mixed_();
  } else if (problem == std::string("nonlinear")) {
    solve_nonlinear_();
  } else {
    throw std::logic_error("Unknow problem type");
  }
  for (auto &&fieldName : manager_.sampleFields()) {
    sample(fieldName);
  }
}

void SimpleModel::solve_linear_elasticity_() {
  add_fem_variable("u", mfu_);
  getfem::add_isotropic_linearized_elasticity_brick_pstress(*this, mim_, "u", "Eyoung", "nu", size_type(-1));
  set_Neumann_data_();
  set_Dirichlet_data_();
  gmm::iteration iter(1E-7, 1, 200);
  iter.init();
  getfem::standard_solve(*this, iter);
}

void SimpleModel::solve_nonlinear_with_bricks_() {
  // main uknown (displacement);
  add_fem_variable("u", mfu_);
  add_initialized_fixed_size_data("params", material_.params());
  getfem::add_finite_strain_elasticity_brick(*this, mim_, material_.law_name(), "u", "params");
  if (false == material_.is_compressible()) {
    add_fem_variable("p", mfp_);
    getfem::add_finite_strain_incompressibility_brick(*this, mim_, "u", "p");
  }
  set_Neumann_data_();
  set_Dirichlet_data_();
  gmm::iteration iter;

  int nb_step = manager_.nb_steps();
  size_type maxit = manager_.max_nb_iter();
  double dt = manager_.solver_dt();

  double residual = manager_.residual_eps();
  for (int step = 0; step < nb_step; ++step) {
    double factor = (step+1.)*dt;
    update_Neumann_data_(factor);
    update_Dirichlet_data_(factor);
    int noisy = 1;
    announce_step_(std::cout, step, factor);
    if (manager_.writeRHS()) {
      assembly(BUILD_RHS);
      write_rhs_();
    }  
    iter = gmm::iteration(residual, noisy, maxit ? maxit : 4000);
    getfem::standard_solve(*this, iter);
  }
  std::cerr << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;
  std::cerr << "Iterations converged: " << iter.converged() << std::endl;
}

void SimpleModel::announce_step_(std::ostream &out, int step, double factor) const {
  out <<"---------------------------------------" << std::endl;
  out <<"Step : " << step << " load factor " << factor << std::endl;
}

void SimpleModel::solve_nonlinear_() {
  solve_nonlinear_with_bricks_();
}

void SimpleModel::solve_linear_elasticity_mixed_() {
  add_fem_variable("u", mfu_);
  add_fem_variable("p", mfp_);
  getfem::add_linear_term(*this, mim_, "2*mu*Sym(Grad_u):Grad_Test_u + p*Trace(Grad_Test_u) + Test_p*Trace(Grad_u)-p*Test_p/lambda");
  set_Neumann_data_();
  set_Dirichlet_data_();
  gmm::iteration iter(1E-7, 1, 200);
  iter.init();
  getfem::standard_solve(*this, iter);
}

const std::vector<std::string>& SimpleModel::variable_names() const {
  variableNames_.clear();
  for (auto iter =variables.begin(); iter != variables.end(); ++iter) {
    if (iter->second.is_variable) {
      variableNames_.push_back(iter->first);
    }
  }  
  return variableNames_;
}

} // namespace sof
