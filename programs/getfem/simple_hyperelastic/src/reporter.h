// Copyright (c) 2018 Roman Putanowicz
//! @brief Managing reports.

#pragma once

#include <string>
#include <iostream>

namespace sof {

class Reporter {
public:
  Reporter();
  void section_start(const std::string &name);
  void section_end();
private:
  std::ostream &out_;
  std::string sectionName_;
  std::string startLineH_;
  std::string startLineT_;
  std::string endLineH_;
  std::string endLineT_;
};

} // namespace sof
