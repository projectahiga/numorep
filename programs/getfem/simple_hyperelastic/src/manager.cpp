//! @brief Define Manager class for managaing application.

#include "errors.h"
#include "manager.h"
#include "material.h"

#include <tclap/CmdLine.h>
#include <easylogging++.h>
#include <string>
#include <sstream>
#include <queue>
#include <utility>
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ini_parser.hpp>

namespace sof {

std::string trim(std::string const& str)
{
    if(str.empty())
        return str;

    std::size_t firstScan = str.find_first_not_of(' ');
    std::size_t first     = firstScan == std::string::npos ? str.length() : firstScan;
    std::size_t last      = str.find_last_not_of(' ');
    return str.substr(first, last-first+1);
}

void split(std::vector<std::string> &elems, const std::string &s, char delim) {
  std::stringstream ss(s);
  std::string item;
  while (std::getline(ss, item, delim)) {
    elems.push_back(std::move(trim(item)));
  }
}

template <typename T>
void put_vector(std::vector<T> &vec, pt::ptree &root,
               const std::string &key) {
  pt::ptree vecNode;
  for (auto &&v : vec) {
    pt::ptree cell;
    cell.put_value(v);
    vecNode.push_back(std::make_pair("", cell));
  }
  root.add_child(key, vecNode);
}

boost::property_tree::ptree
mergePropertyTrees( const boost::property_tree::ptree& rptFirst, const boost::property_tree::ptree& rptSecond )
{
  // Take over first property tree
  boost::property_tree::ptree ptMerged = rptFirst;

  // Keep track of keys and values (subtrees) in second property tree
  std::queue<std::string> qKeys;
  std::queue<boost::property_tree::ptree> qValues;
  qValues.push( rptSecond );

  // Iterate over second property tree
  while( !qValues.empty() )
  {
    // Setup keys and corresponding values
    boost::property_tree::ptree ptree = qValues.front();
    qValues.pop();
    std::string keychain = "";
    if( !qKeys.empty() )
    {
      keychain = qKeys.front();
      qKeys.pop();
    }

    // Iterate over keys level-wise
    for ( const boost::property_tree::ptree::value_type& child : ptree )
    {
      // No "." for first level entries
      std::string s;
      if( keychain != "" ) s = keychain + "." + child.first.data();
      else s = child.first.data();
      // Leaf
      if( child.second.size() == 0 )
      {
        // Put into combined property tree
        ptMerged.put( s, child.second.data() );
      } else if ( child.first.data() == std::string("value")
                || std::string(child.first.data()).substr(0,3) == std::string("pt_")) {
        std::vector<double> v = get_vector<double>(child.second, "");
        put_vector(v, ptMerged, s);
      }
      // Subtree
      else
      {
        // Put keys (identifiers of subtrees) and all of its parents (where present)
        // aside for later iteration. Keys on first level have no parents
        if( keychain != "" )
          qKeys.push( keychain + "." + child.first.data() );
        else
          qKeys.push( child.first.data() );

        // Put values (the subtrees) aside, too
        qValues.push( child.second );
      }
    }  // -- End of foreach
  }  // --- End of while

  return ptMerged;
}

FieldDef& FieldDef::operator=(const std::string &str)
{
  auto delim = std::string("=");
  auto start = 0U;
  auto end = str.find(delim);
  if (end != std::string::npos)
  {
    name = str.substr(start, end - start);
    start = end+1;
    end = str.size();
    expression = str.substr(start, end-start);
  }
  else
  {
    throw TCLAP::ArgParseException(str + " : missing equal sign in file specification");
  }
  return *this;
}

std::ostream& FieldDef::print(std::ostream &out) const
{
  out << "\"" << name << "=" << expression << "\"";
  return out;
}

std::ostream& operator<<(std::ostream &os, const FieldDef &f)
{
    return f.print(os);
}

} // namespace sof

namespace TCLAP
{

template<>
struct ArgTraits<sof::FieldDef> {
    typedef StringLike ValueCategory;
};

} // namespace TCLAP

namespace sof {

void Manager::list_probe_points(std::ostream &out) const {
  size_t N = probe_points_count();
  std::vector<double> pt;
  for (size_t i=0; i<N; ++i) {
    get_probe_point(i, pt);
    out << boost::format("[%f, %f]\n") % pt[0] % pt[1];
  }
}

size_t Manager::probe_points_count() const {
  auto& node = props_.get_child("analysis.probe_points");
  node.size();
}

bool Manager::are_parametric_probes() const {
  auto type = props_.get<std::string>("analysis.probe_points_type");
  if (type == std::string("parametric")) {
    return true;
  } else if (type == std::string("physical")) {
    return false;
  } else {
    throw std::logic_error("Invalid probe point type");
  }
}

void Manager::set_request_(const std::string &arg) {
  const char *validRequests[] = {"analyze", "solve"};
  const int nr = sizeof(validRequests)/sizeof(char*);
  for (int i=0; i<nr; ++i) {
    if (arg == std::string(validRequests[i])) {
      requestType_ = std::string(validRequests[i]);
      return;
    }
  }
  throw TCLAP::CmdLineParseException("Invalid request type", "request");
}

Manager::RHSType Manager::toRHSType(const std::string &name) const {
  if (name == std::string("compact")) {
    return RHSType::COMPACT;
  } else if (name == std::string("full")) {
    return RHSType::FULL;
  } else {
    throw std::logic_error("Invalid value of string for RHSType");
  }
}


bool Manager::parseCmdArgs(int argc, char *argv[]) {
  auto isOK = true;
  try {
    std::vector<std::string> allowed;
    allowed.push_back("compact");
    allowed.push_back("full");
    TCLAP::ValuesConstraint<std::string> allowedRHS( allowed );

    TCLAP::CmdLine cmd("Simulation of hyperelastic material model", ' ', "0.1");

    TCLAP::ValueArg<std::string> requestArg("r", "request", "request type", false, "solve", "string", cmd);
    TCLAP::MultiArg<FieldDef>    fieldArg("f", "field", "field", false, "FieldDef", cmd);
    TCLAP::ValueArg<std::string> fieldsToSampleArg("", "sample", "fields to samle", false, "", "string", cmd);
    TCLAP::ValueArg<std::string> rangesToShowArg("", "ranges", "ranges to show", false, "", "string", cmd);
    TCLAP::UnlabeledValueArg<std::string> infileArg("infile", "input file name", false, "", "string", cmd);
    TCLAP::ValueArg<std::string> ufileArg("u", "ufile", "displacement file name", false, "", "string", cmd);
    TCLAP::SwitchArg meshInfoArg("i", "meshinfo", "print mesh info", cmd, false);
    TCLAP::ValueArg<std::string> writeRHSArg("", "write-rhs", "write RHS", false, "compact", &allowedRHS, cmd);
    TCLAP::SwitchArg doEchoArg("e", "echo", "print manager echo", cmd, false);
    TCLAP::SwitchArg noLogArg("s", "nolog", "switch off log messages",cmd,  false);
    TCLAP::SwitchArg doTestingArg("", "selftest", "run program in self-testing mode", cmd, false);
    TCLAP::SwitchArg listVariablesArg("", "listvariables", "list model variables", cmd, false);
    TCLAP::SwitchArg listDofsArg("", "listdofs", "list model dofs", cmd, false);
    TCLAP::SwitchArg listBricksArg("", "listbricks", "list model bricks", cmd, false);
    TCLAP::SwitchArg listModelArg("", "listmodel", "list model variables and data", cmd, false); 
    cmd.parse(argc, argv);

    doTesting_ = doTestingArg.getValue();
    set_request_(requestArg.getValue());
    if (requestType_ == std::string("analyze")) {
      haveUFile = ufileArg.isSet();
      if (haveUFile) {
        ufieldFile = ufileArg.getValue();
      }
    }
    writeInfo = meshInfoArg.getValue();
    writeRHS_ = writeRHSArg.isSet();
    rhsType_ = toRHSType(writeRHSArg.getValue());
    doEcho = doEchoArg.getValue();
    noLog = noLogArg.getValue();
    doList_["variables"] = listVariablesArg.getValue();
    doList_["dofs"] = listDofsArg.getValue();
    doList_["bricks"] = listBricksArg.getValue();
    doList_["model"] = listModelArg.getValue();
    doList_["ranges"] = rangesToShowArg.isSet();
    if (!doTesting_) {
      inFile = infileArg.getValue();
      read(inFile);
    }
    update_fields_(fieldArg.getValue());
    split(sampleFields_, fieldsToSampleArg, ',');
    split(rangesToShow_, rangesToShowArg, ',');
  } catch(TCLAP::ArgException &e) {
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
    isOK = false;
    exitStatus = EXIT_FAILURE;
  }
  return isOK;
}

Manager::Manager() {
  setup_props_();
}

const std::vector<std::string> & Manager::rangesToShow() const {
  return rangesToShow_;
}

const std::vector<std::string> & Manager::sampleFields() const {
  return sampleFields_;
}

bool Manager::do_testing() const {
  return doTesting_;
}

bool Manager::do_list(const std::string &key) const {
  auto iter = doList_.find(key);
  if (iter != doList_.end()) {
    return iter->second;
  } else {
    std::stringstream oss;
    oss << "Invalid listing request key: " << key;
    throw ManagerError(oss.str());
  }
}

void Manager::setup_props_() {
   props_.put("fem_u",   "FEM_QK(2,1)");
   props_.put("fem_p",   "FEM_QK(2,0)");
   props_.put("fem_data"  ,"FEM_QK(2,1)");
   props_.put("integration", "IM_NC_PARALLELEPIPED(2,6)");
   props_.put("material.type", "elastic");
   props_.put("problem.type", "displacement");
   props_.put("mesh.type", "simple");
   props_.put("mesh.regular.nx", 5);
   props_.put("mesh.regular.ny", 5);
   props_.put("mesh.cooks_membrane.H", 44.0);
   props_.put("mesh.cooks_membrane.W", 48.0);
   props_.put("mesh.cooks_membrane.HL", 16.0);
   props_.put("mesh.cooks_membrane.TD", 16.0);
   props_.put("domain.Lx", 1.0);
   props_.put("domain.Ly", 1.0);
   props_.put("bc.south.type", "fixed");
   props_.put("bc.east.type", "free");
   props_.put("bc.north.type", "free");
   props_.put("bc.west.type", "free");
   props_.put("analysis.fields.u.expression", "x,0");
   props_.put("analysis.fields.u.type", "expression");
   props_.put("analysis.probe_points_type", "physical");
   props_.put("solver.nb_steps", 10);
   props_.put("solver.dt", 0.1); 
   props_.put("solver.max_nb_iter", 100);
   props_.put("solver.residual", 1e-8);
//   auto probe = std::vector<double>{0.0, 0.0};
   pt::ptree child;
   auto& node = props_.get_child("analysis");
   node.push_back(std::make_pair("probe_points", child));
//   put_vector(probe, props_, "analysis.probe_points.0");

}

void Manager::update_fields_(const std::vector<FieldDef> &fields) {
  std::string key = std::string("analysis.fields");
  auto& node = props_.get_child(key);
  for (auto &v : fields) {
    std::string path = v.name + std::string(".expression");
    node.put(path, v.expression);
  }
}

// This checks for keys not for paths to children.
void Manager::check_prop_exists_(const std::string &key) const {
  if (props_.find(key) == props_.not_found()){
    std::stringstream oss;
    oss << "ERROR: Cannot find property : " << key;
    throw std::logic_error(oss.str());
  }
}

USourceType Manager::displacement_source() const {
  auto type = props_.get<std::string>("analysis.fields.u.type", std::string("undefined"));
  USourceType utype;
  if (type == std::string("expression")) {
    return USourceType::EXPRESSION;
  } else if (type == std::string("simple_shear")) {
    return USourceType::SIMPLE_SHEAR;
  } else {
    std::stringstream oss;
    oss << "Unsupported type of displacement field at analysis.fields.u.type : " << type;
    throw std::logic_error(oss.str());
  }
}

std::string Manager::u_field_expr() const {
  return props_.get<std::string>("analysis.fields.u.expression");
}

std::string Manager::mesh_type() const {
  return props_.get<std::string>("mesh.type");
}

std::string Manager::problem_type() const {
  return props_.get<std::string>("problem.type");
}

double Manager::cooks_membrane(const std::string &param) const {
  const std::vector<std::string> valid{"W", "H", "HL", "TD"};
  if (std::find(std::begin(valid), std::end(valid), param) == std::end(valid)) {
    std::stringstream oss;
    oss << "Inalid name of Cook's membrane parameter : " << param;
    throw std::logic_error(oss.str());
  }
  auto& node = props_.get_child("mesh.cooks_membrane");
  return node.get<double>(param);
}

unsigned int Manager::mesh_resolution(const int dir) const {
  const std::vector<std::string> dirs = {"nx", "ny"};
  const std::string path = std::string("mesh.regular.") + dirs.at(dir);
  return props_.get<unsigned int>(path);
}

double Manager::shear_k() const {
  auto expr = props_.get<std::string>("analysis.fields.u.simple_shear.k");
  parser_.SetExpr(expr);
  return parser_.Eval();
}

double Manager::length(const int dir) const {
  const std::vector<std::string> dirs = {"Lx", "Ly"};
  const std::string path = std::string("domain.") + dirs.at(dir);
  return props_.get<double>(path);
}

std::string Manager::fem_type(const std::string fieldname) const {
  std::string path = std::string("fem_") + fieldname;
  return props_.get<std::string>(path);
}

std::string Manager::integration_type() const {
  return props_.get<std::string>("integration");
}

bool Manager::has_mu_lambda() const {
  auto& node = props_.get_child("material");
  return (node.find("mu") != node.not_found()
           && node.find("lambda") != node.not_found());
}

bool Manager::is_material_compressible() const {
  auto& node = props_.get_child("material");
  return node.get("compressible", true);
}

std::string Manager::material_law() const {
  std::string lawName = props_.get<std::string>("material.law");
  if (false == Material::is_valid_law(lawName)) {
    std::stringstream oss;
    oss << "Invalid material law : " << lawName;
    throw std::logic_error(oss.str());
  }
  return lawName;
}

bool Manager::has_E_nu() const {
  auto& node = props_.get_child("material");
  return (node.find("E") != node.not_found()
           && node.find("nu") != node.not_found());
}

double Manager::E() const {
  return props_.get<double>("material.E");
}

double Manager::nu() const {
  return props_.get<double>("material.nu");
}

double Manager::lambda() const {
  return props_.get<double>("material.lambda");
}

double Manager::mu() const {
  return props_.get<double>("material.mu");
}

double Manager::solver_dt() const {
  auto& node = props_.get_child("solver");
  auto expr = node.get<std::string>("dt");
  parser_.SetExpr(expr);
  return parser_.Eval();
}

size_t Manager::nb_steps() const {
  auto& node = props_.get_child("solver");
  return node.get<size_t>("nb_steps");
}

size_t Manager::max_nb_iter() const {
  auto& node = props_.get_child("solver");
  return node.get<size_t>("max_nb_iter");
}

double Manager::residual_eps() const {
  auto& node = props_.get_child("solver");
  return node.get<double>("residual");
}

std::string Manager::request_type() const {
  return requestType_;
}

BCData Manager::bc_data(const std::string &side) const {
  std::string key = std::string("bc.")+side;
  auto& node = props_.get_child(key);
  std::string type = node.get<std::string>("type");
  if (   type == std::string("fixed")
      || type == std::string("free")
      || type == std::string("sliding")) {
    return BCData(type);
  } else if (type == std::string("pressure")) {
    double v[2];
    v[0] = 0.0;
    v[1] = -1.0 * node.get<double>("value");
    std::cerr << "Retruning pressure bc for side : " << side << " \n";
    return BCData(type, 2, v);
  } else if (type == std::string("traction")) {
    auto v = get_vector<double>(node, "value");
    return BCData(type, v.size(), &v[0]);
  } else if (type == std::string("ramp_traction")) {
    auto vA = get_vector<double>(node, "valueA");
    auto vB = get_vector<double>(node, "valueB");
    auto T = get_vector<double>(node, "treshold");
    auto refPoint = get_vector<double>(node, "refpoint");
    auto control = node.get<std::string>("control");  
    return BCData(type, control, T[0], T[1], refPoint[0], refPoint[1], 
                  vA.size(), &vA[0], &vB[0]);
  } else {
    std::stringstream oss;
    oss << "Invalid type of BC : " << type;
    throw std::logic_error(oss.str());
  }
}

bool Manager::write(const bf::path &path, bool overwrite) const {
  pt::write_json(path.string(), props_);
  return true;
}

void Manager::echo(std::ostream &out) const {
  out << "Manager settings: " << std::endl;
  pt::write_json(out, props_);
  out << "--------------------------" << std::endl;
}

void Manager::verify_() const {
  bool state = true;
  state = state && (has_mu_lambda() || has_E_nu());
  if (state != true) {
    throw std::logic_error("Invalid configuration: missing either (mu, lambda) or (E, nu) parameters");
  }
}

void Manager::setup_expressions_parser_() {
  auto& node = props_.get_child("solver");
  const auto nb_steps = node.get<size_t>("nb_steps");
  parser_.DefineConst("nb_steps", static_cast<double>(nb_steps));
}

bool Manager::read(const bf::path &path) {
  pt::ptree usertree;
  pt::read_json(path.string(), usertree);
  props_ = mergePropertyTrees(props_, usertree);
  verify_();
  setup_expressions_parser_();
  return true;
}

} // namespace sof


