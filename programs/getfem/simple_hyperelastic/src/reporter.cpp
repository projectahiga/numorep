// Copyright (c) 2018 Roman Putanowicz

#include "reporter.h"

namespace sof {

Reporter::Reporter() : out_(std::cout) {
  char lc='=';
  startLineH_ = std::string(8, lc);
  startLineT_ = std::string(8, lc);
  endLineH_ = std::string(8, lc);
  endLineT_ = std::string(8+2, lc);
}
void Reporter::section_start(const std::string &name) {
  sectionName_ = name;
  out_ << startLineH_ << " Start : " << sectionName_ << " " 
       << startLineT_ << std::endl;
}

void Reporter::section_end() {
  out_ << startLineH_ << " End : " << sectionName_ << " " 
       << startLineT_ << std::endl;
  sectionName_ = std::string("");
}

} // namespace sof
