// Copyright (c) 2018 Roman Putanowicz
//! @brief Geometric utilities.

#include "geometry.h"
#include <vector>

#include <gmm/gmm.h>

/* some GetFEM++ types that we will be using */
using bgeot::base_node;  /* geometrical nodes(derived from base_small_vector)*/

namespace sof {

const std::vector<enum Side> &squareSides() {
 static std::vector<enum Side> v = {SOUTH, EAST, NORTH, WEST};
 return v;
}

const std::vector<const char *> &sideNames() {
  static std::vector<const char *> v = {"south", "east", "north", "west"};
  return v;
}


bool onSide(Side side, base_node un) {
  const auto tol = 1.0E-7;
  switch(side) {
    case SOUTH :
      return gmm::abs(un[1] + 1.0) < tol;
    case EAST :
      return gmm::abs(un[0] - 1.0) < tol;
    case NORTH :
      return gmm::abs(un[1] - 1.0) < tol;
    case WEST :
      return gmm::abs(un[0] + 1.0) < tol;
  }
}

} // namespace sof
