// Copyright (c) 2018 Roman Putanowicz
//! @brief Implement functor object that wrapps global functions.

#include "global_function.h"

#include <boost/format.hpp>

#include <algorithm>
#include <stdexcept>

namespace sof
{

std::vector<double> VectorFunctionBase::at(double x, double y, double z) const {
  pt_[0] = x; pt_[1] = y; pt_[2] = z;
  return eval_();
}

std::vector<double> VectorFunctionBase::at(size_t ncoords, const double *coords) const {
  for (size_t i=0; i<ncoords; ++i) pt_[i] = coords[i];
  return eval_();
}

size_t VectorFunctionBase::dim() const {
  return dim_;
}

const std::string VectorFunctionBase::name() const {
  return name_;
}

VectorFunctionBase::VectorFunctionBase(const std::string &name, size_t dim) :
  name_(name), dim_(dim) {
}

SimpleShearDisplacement::SimpleShearDisplacement(const std::string &name,
  double k, size_t dim) : VectorFunctionBase(name, dim), k_(k) {
}

std::vector<double> SimpleShearDisplacement::eval_() const {
  std::vector<double> u(dim_, 0.0);
  u[0] = k_*pt_[1];
  return u;
}

std::string SimpleShearDisplacement::expression() const {
  std::stringstream oss;
  oss <<  boost::format("x*%f, 0") % k_;
  return oss.str();
}

ToCooksMembraneDisplacement::ToCooksMembraneDisplacement(double w, double h, double hl, double td) :
  VectorFunctionBase("ToCooksMembrane", 2),
  w_(w), h_(h), hl_(hl), td_(td) {
}

std::string ToCooksMembraneDisplacement::expression() const {
  return std::string("x*W-x, y*H-(H-HL)*x*y+(H+TH-HL)*x-y");
}


std::vector<double> ToCooksMembraneDisplacement::eval_() const {
  std::vector<double> u(dim_, 0.0);
  const double x = pt_[0];
  const double y = pt_[1];
  u[0] = x*w_-x;
  u[1] = y*h_-(h_-hl_)*x*y + (h_+td_-hl_)*x - y;
  return u;
}

GlobalVectorFunction::GlobalVectorFunction(const std::string &name,
                               const std::string &expression) : VectorFunctionBase(name, 1) {
  parser_.DefineVar("x", &pt_[0]);
  parser_.DefineVar("y", &pt_[1]);
  parser_.DefineVar("z", &pt_[2]);
  parser_.SetExpr(expression);
  dim_ = 1+std::count(expression.begin(), expression.end(), ',');
}

std::vector<double> GlobalVectorFunction::eval_() const {
  int d = dim_;
  double *v = parser_.Eval(d);
  std::vector<double> r(dim_);
  std::copy(v, v+dim_, r.begin());
  return r;
}

GlobalFunction::GlobalFunction(const std::string &name,
                               const std::string &expression) : name_(name)
{
  parser_.DefineVar("x", &pt_[0]);
  parser_.DefineVar("y", &pt_[1]);
  parser_.DefineVar("z", &pt_[2]);
  parser_.SetExpr(expression);
}

double GlobalFunction::at(double x, double y, double z) const
{
  pt_[0] = x;
  pt_[1] = y;
  pt_[2] = z;
  return parser_.Eval();
}

double GlobalFunction::at(size_t ncoords, const double *coords) const
{
  for (size_t i=0; i<ncoords; ++i) pt_[i] = coords[i];
  return parser_.Eval();
}
} // namespace sof
