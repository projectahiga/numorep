// Copyright (c) 2018 Roman Putanowicz
//! @brief Class representing material properties.

#pragma once

#include <map>
#include <string>

namespace sof {

class Manager;

class Material {
  public:
    Material(const Manager &manager);
    double      E() const { return get_constant_("E"); }
    double     mu() const { return get_constant_("mu"); }
    double lambda() const { return get_constant_("lambda"); }
    double     nu() const { return get_constant_("nu"); }
    bool is_compressible() const { return isCompressible_; }
    const std::string& law_name() const { return lawName_; }
    const std::vector<double>& params() const { return params_; }
    static bool is_valid_law(const std::string &name);
  private:
    static const std::vector<std::string> validLaws_;
    void setup_(const Manager &manager);
    void setup_from_mu_lambda_(const Manager &manager);
    void setup_from_E_nu_(const Manager &manager);
    void setup_law_name_and_compressibility_(const Manager &manager);
    void setup_material_law_params_(const Manager &manager);
    double get_constant_(const std::string &name) const;
    std::map<std::string, double> constants_;
    //! material law params
    std::vector<double> params_;
    double nuLimit_ = 0.4999;
    bool isCompressible_;
    //! Constitutive law name
    std::string lawName_;
};

} // namespace sof
