// Copyright (c) 2018 Roman Putanowicz

#pragma once
#include <utility>
#include <iterator>

namespace sof {

template <typename T>
std::pair<typename T::const_iterator, typename T::const_iterator> dof_minmax(const T &vec, int component, size_t qdim) {
  auto ci = std::begin(vec);
  auto e = std::end(vec);
  std::advance(ci, component); 
  auto res = std::make_pair(ci, ci);
  while (std::distance(ci, e) > qdim) {
    std::advance(ci, qdim);
    if (*res.first > *ci) { res.first = ci; }
    if (*res.second < *ci) { res.second = ci; };
  } 
  return res;
}

} // namespace sof
