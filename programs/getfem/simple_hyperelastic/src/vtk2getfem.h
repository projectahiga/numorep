// Copyright (c) 2018 Roman Putanowicz
//! @brief Function to transfer data from VTK to GetFEM.

#pragma once

#include <vtkSmartPointer.h>
#include <vtkUnstructuredGrid.h>
#include <getfem/getfem_mesh.h>
#include <getfem/getfem_fem.h>
#include <getfem/getfem_models.h>

#include <string>

namespace v2g {
  void copyMesh(vtkSmartPointer<vtkUnstructuredGrid> vtkGrid, getfem::mesh & gmesh);
  void interpolateField(const std::string &name, vtkSmartPointer<vtkUnstructuredGrid> pMesh,
                        const getfem::mesh_fem &fem, std::vector<double> &V);
  std::string vectorsName(vtkSmartPointer<vtkUnstructuredGrid> vtkGrid);
  void readModel(getfem::mesh &mesh, getfem::model &model, const std::string &fileName);

} // namespace v2g
