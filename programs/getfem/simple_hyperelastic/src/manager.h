// Copyright (c) 2018 Roman Putanowicz
//! @brief Define Manager class for managaing application.

#pragma once

#include "bc.h"
#include "reporter.h"

#include <muParser.h>

#include <boost/filesystem.hpp>
#include <boost/format.hpp>
#include <boost/property_tree/ptree.hpp>

#include <iostream>
#include <string>
#include <vector>

namespace pt=boost::property_tree;
namespace bf=boost::filesystem;

namespace sof {

template <typename T>
std::vector<T> get_vector(pt::ptree const& pt, pt::ptree::key_type const& path)
{
    std::vector<T> r;
    for (auto& item : pt.get_child(path))
       r.push_back(item.second.get_value<T>());
    return r;
}

struct FieldDef {
  std::string name = std::string("data");
  std::string expression = std::string("x");
  FieldDef(const std::string &fieldName, const std::string &fieldExpression) : name(fieldName), expression(fieldExpression){}
  FieldDef() = default;
  // operator= will be used to assign to the FieldDef
  FieldDef& operator=(const std::string &str);
  std::ostream& print(std::ostream &out) const;
};

std::ostream& operator<<(std::ostream &os, const FieldDef &f);

enum class USourceType {
  EXPRESSION,
  SIMPLE_SHEAR
};

class Manager {
public:
  enum class RHSType {
    COMPACT, 
    FULL
  };
  Manager();
  bool parseCmdArgs(int argc, char *argv[]);

  bool read(const bf::path &path);
  bool write(const bf::path &path, bool overwrite=false) const;

  void echo(std::ostream &out = std::cout) const;

  std::string request_type() const;

  double shear_k() const;
  BCData bc_data(const std::string &side) const;
  std::string fem_type(const std::string fieldname) const;
  std::string problem_type() const;
  std::string integration_type() const;
  std::string material_law() const;
  bool is_material_compressible() const;
  double lambda() const;
  double mu() const;
  double E() const;
  double nu() const;
  //! @ Return true if mu and lambda parameters are specified.
  bool has_mu_lambda() const;
  //! @ Return true if E and nu parameters are specified.
  bool has_E_nu() const;
  //! @brief Return regular mesh resolution in given direction
  unsigned int mesh_resolution(const int dir) const;
  //! @brief Return domain linear dimension in given direction.
  double length(const int dir) const;
  std::string mesh_type() const;
  //! @brief Return value of dimension parameter of Cooks' membrane
  //! Walid parameter names: W, H, HL, TD
  double cooks_membrane(const std::string &param) const;
  //! @brief Type of displacement field source
  USourceType displacement_source() const;

  //! @brief Return expression for displacement field u
  std::string u_field_expr() const;

  //! @brief Number of probe points
  size_t probe_points_count() const;

  //! @brief Return true if probe poins are parametric
  bool are_parametric_probes() const;

  //! @brief Increment of solver control parameter
  double solver_dt() const;

  //! @brief Number of steps for nonlinear solver
  size_t nb_steps() const;

  //! @biref Residual tolerance
  double residual_eps() const;


  //! @biref Max number of iterations per step in nonlinear solver
  size_t max_nb_iter() const;

  //! @brief Return vector of variables and their components for which to
  //! display ranges
  const std::vector<std::string>& rangesToShow() const;

  //! @brief Return vector of names of fields to sample.
  const std::vector<std::string>& sampleFields() const;

  //! @brief Retrun true if writing RHS requested
  bool writeRHS() const { return writeRHS_;} 
  
  //! @biref Return output type for RHS.
  RHSType rhs_type() const {return rhsType_;}

  RHSType toRHSType(const std::string &name) const;

  Reporter& reporter() const { return reporter_; }

  template <class T>
  void get_probe_point(size_t id, T &v) const {
    std::string path =  boost::str(boost::format("probe_points.pt_%d") % id);
    auto& node = props_.get_child("analysis");
    auto pt = get_vector<double>(node, path);
    v.resize(pt.size());
    std::copy(pt.begin(), pt.end(), v.begin());
  }

  bool do_list(const std::string &key) const;

  //! @brief Return true if program should be run in selftesting mode.
  bool do_testing() const;

  void list_probe_points(std::ostream &out) const;

  std::string inFile;
  bool noLog = false;
  bool writeInfo = false;
  bool doEcho = false;
  int exitStatus = EXIT_SUCCESS;
  std::string ufieldFile;
  bool haveUFile = false;

private:
  using ConfigData = pt::ptree;
  using ConfigDataIter = pt::ptree::const_iterator;
  ConfigData props_;
  std::string requestType_;
  std::vector<std::string> sampleFields_;
  std::vector<std::string> rangesToShow_;
  std::map<std::string, bool> doList_;
  mutable mu::Parser parser_;
  bool writeRHS_ = false;
  bool doTesting_ = false;
  RHSType rhsType_ = RHSType::COMPACT;
  mutable Reporter reporter_;
  void setup_props_();
  void setup_expressions_parser_();
  void set_request_(const std::string &arg);
  void update_fields_(const std::vector<FieldDef> &fields);
  void check_prop_exists_(const std::string &key) const;
  void verify_() const;
};

} // namespace sof
