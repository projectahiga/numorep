// Copyright (c) 2018 Roman Putanowicz
//! @brief Utilitis for various parsing tasks.

#pragma once

#include <string>
#include <utility>

namespace sof {

//! Splint string of the form: 'name(index)' into name and index part
//! and return it as string and integer(size_t) pair.
std::pair<std::string, int> parseNameIndex(const std::string &text);

//! Convert string containing proper non-negative integer or one of
//! 'XxYyZz' characters into size_t value.
int str2index(const std::string &str);

} // namespace sof
