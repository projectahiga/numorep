// Copyright (c) 2018 Roman Putanowicz
//! @brief Boundary condition data

#pragma once

#include <string>
#include <vector>
#include <getfem/bgeot_small_vector.h>

namespace sof {

class BCData {
public:
  enum Type {
    DISPLACEMENT = 0,
    FIXED,
    FREE,
    PRESSURE,
    SLIDING,
    TRACTION,
    RAMP_TRACTION
  };

  static const std::vector<const char *> validNames;

  BCData(const std::string &type, int size=0, const double *v=nullptr);
  BCData(const std::string &type, const std::string &control, 
         double thresholdMin, double tresholdMax, 
         double x, double y, int size=0,
         const double *vA=nullptr, const double *vB=nullptr);
  const std::string &type_name() const;
  enum Type type() const;
  const std::vector<double> &value() const;
  const std::vector<double> &value(const std::vector<double> &driver) const;
  const bgeot::base_node& get_ref_point() const;
  void set_ref_point(const bgeot::base_node &refPoint);

private:
  void setup_(const std::string &type, int size, const double *v);
  void setup_(const std::string &type, int size, const double *vA, const double *vB);
  void setup_type_(const std::string &type);
  void setup_value_(int size, const double *v, std::vector<double> &val);

  std::string type_name_;
  mutable std::vector<double> value_;
  std::vector<double> vA_;
  std::vector<double> vB_;
  std::array<double, 2> treshold_ = {{0.0,0.0}};
  std::string control_; 
  Type type_;
  bgeot::base_node refPoint_ = {0.0, 0.0};
};

} // namespace sof
