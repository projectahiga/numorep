// Copyright (c) 2018 Roman Putanowicz
//! @brief Computational model.

#pragma once

#include <vector>

#include <getfem/bgeot_small_vector.h>

namespace sof {

enum Side {SOUTH=0, EAST, NORTH, WEST};
enum Dir {X_DIR=0, Y_DIR};

const std::vector<enum Side> &squareSides();
const std::vector<const char*> &sideNames();

bool onSide(Side side, bgeot::base_node un);

} // namespace sof
