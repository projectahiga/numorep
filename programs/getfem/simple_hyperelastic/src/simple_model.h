// Copyright (c) 2018 Roman Putanowicz
//! @brief Computational model.

#pragma once

#include "material.h"

#include <getfem/getfem_models.h>
#include <getfem/getfem_mesh.h>
#include <getfem/getfem_mesh_fem.h>
#include <getfem/getfem_mesh_im.h>
#include <getfem/dal_bit_vector.h>

namespace sof {

class Manager;

struct ComputedField {
  ComputedField(const getfem::mesh_fem &fem_, const std::string expr_,
                const std::string &description_=std::string("")) : fem(fem_), expr(expr_),
    description(description_) {}
  const getfem::mesh_fem &fem;
  std::string expr;
  std::string description;
};

class SimpleModel : public getfem::model {
  public:
    SimpleModel(const Manager &manager);
    void write_mesh();
    void write_solution();
    void solve();
    void analyze();
    void sample(const std::string &fieldName);
    void write_u_dofs(std::ostream &out, bool writeCoords=true) const;
    void listdofs(std::ostream &out) const;
    void listvariables(std::ostream &out) const;
    void listranges(std::ostream &out, const std::vector<std::string> &rangesToShow) const;
    const std::vector<std::string>& variable_names() const;
  private:
    void announce_step_(std::ostream &out, int step, double factor) const;
    void configure_();
    //! @brief Cooks' membrane mesh
    void build_cooks_membrane_mesh_();
    //! @brief Regualr structured quad mesh in Lx by Ly square with Nx times Ny quads.
    void build_regular_mesh_();
    //! @brief Simple 4-quad mesh in unit square.
    void build_simple_patch_mesh_();
    void build_parametric_mesh_();
    void build_mesh_();
    void compute_displacement_();
    void displacement_from_expression_();
    void displacement_simple_shear_();
    void print_probe_point_(std::ostream &out, size_t index) const;
    void set_boundary_regions_();
    void set_data_();
    void set_fem_and_im_();
    void set_Dirichlet_data_();
    void set_Neumann_data_();
    void solve_linear_elasticity_();
    void solve_linear_elasticity_mixed_();
    void solve_nonlinear_();
    void solve_nonlinear_with_bricks_();
    void solve_nonlinear_bc_();
    void set_builtInFields_();
    void transform_probe_point_(bgeot::base_node &pt) const;
    void update_data_(double factor, const std::string &dataName,
                      const std::vector<double> &dataSource,
                      const dal::bit_vector &dofs);
    void update_Dirichlet_data_(double factor);
    void update_Neumann_data_(double factor);
    void write_rhs_() const;
    void write_rhs_compact_() const;
    void write_rhs_full_() const;
    const Manager &manager_;
    bgeot::pgeometric_trans pgt_; // element geometric transformation
    getfem::mesh mesh_;
    getfem::mesh_im mim_;      // the integration methods
    getfem::mesh_fem mfu_;     // main mesh_fem, for the elastostatic solution
    getfem::mesh_fem mfp_;     // mesh_fem for the pressure.
    getfem::mesh_fem mfrhs_;   // mesh_fem for the right hand side (f(x),..)
    getfem::mesh_fem mfcoef_;  // mesh_fem used to represent pde coefficients
    getfem::mesh_fem mfmat_;   // mesh_fem used for matrices
    getfem::mesh_fem mfsf_;    // mesh_fem used for shape functions evaluation
    getfem::mesh_fem mfsd_;    // mesh_fem used for shape functions derivatives evaluation
    double p_[3];              // elastic coefficients.
    const getfem::size_type N_ = 2; // model dimension
    std::map<std::string, ComputedField> builtInFields_;
    std::vector<double> dirichletData_;
    dal::bit_vector dirichletDofs_;
    std::vector<double> neumannData_;
    dal::bit_vector neumannDofs_;
    mutable std::vector<std::string> variableNames_;
    Material material_;
    SimpleModel(const SimpleModel &other) = delete;
    SimpleModel& operator = (const SimpleModel &other) = delete;
};

} // namespace sof
