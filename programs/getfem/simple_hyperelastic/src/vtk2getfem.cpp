// Copyright (c) 2018 Roman Putanowicz
//! @brief Function to transfer data from VTK to GetFEM.

#include "vtk2getfem.h"

#include <algorithm>
#include <easylogging++.h>
#include <vtkCellType.h>

#include <getfem/getfem_import.h>
#include <getfem/getfem_export.h>
#include <getfem/getfem_mesh.h>
#include <getfem/getfem_mesh_fem.h>
#include <getfem/getfem_mesh_fem_global_function.h>
#include <getfem/getfem_assembling_tensors.h>
#include <sstream>

#include <vtkUnstructuredGrid.h>
#include <vtkUnstructuredGridReader.h>
#include <vtkSmartPointer.h>
#include <vtkPoints.h>
#include <vtkProbeFilter.h>
#include <vtkDataArray.h>
#include <vtkPointData.h>

namespace getfem
{
 struct gf2vtk_dof_mapping : public std::vector<std::vector<unsigned> > {};
 struct gf2vtk_vtk_type : public std::vector<int> {};
 void init_gf2vtk();
}

namespace v2g
{
static const std::vector<unsigned> &
select_vtk_dof_mapping(unsigned t) {
  getfem::gf2vtk_dof_mapping &vtkmaps = dal::singleton<getfem::gf2vtk_dof_mapping>::instance();
  if (vtkmaps.size() == 0) getfem::init_gf2vtk();
  return vtkmaps[t];
}


class CellMapping {
public:
  CellMapping() = default;
  CellMapping(int cellType);
  void setup(int cellType);
  bgeot::pgeometric_trans getTrans() { return ptrans_; }
  void mapConnectivity(vtkIdType npts, const vtkIdType *pts, std::vector<bgeot::size_type> &ind);
  int vtkCellType() { return vtkCellType_; }
private:
  //! GetFEM geometric transformation corresponding to VTK cell
  bgeot::pgeometric_trans ptrans_ = nullptr;
  //! Integer correcponding to GetFEM mapping between cell indices
  int gmaptype_ = 0;
  //! VTK cell type;
  int vtkCellType_ = -1;
  std::vector<unsigned> map_;
};

CellMapping::CellMapping(int cellType)
{
 setup(cellType);
}

void CellMapping::setup(int cellType)
{
  if (vtkCellType_ == cellType) return;
  vtkCellType_ = cellType;
  switch(vtkCellType_)
  {
    case VTK_LINE: // 3
      ptrans_ = bgeot::simplex_geotrans(1,1);
      gmaptype_ = 2;
      break;
    case VTK_TRIANGLE: // 5
      ptrans_  = bgeot::simplex_geotrans(2,1);
      gmaptype_ = 3;
      break;
    case VTK_QUAD: // 9
      ptrans_ = bgeot::parallelepiped_geotrans(2,1);
      gmaptype_ = 5;
      break;
    default:
      throw(std::logic_error("unsupported vtk type"));
  }
  getfem::gf2vtk_dof_mapping &vtkmaps = dal::singleton<getfem::gf2vtk_dof_mapping>::instance();
  if (vtkmaps.size() == 0) getfem::init_gf2vtk();
  map_ = vtkmaps[gmaptype_];
}

void CellMapping::mapConnectivity(vtkIdType npts, const vtkIdType *pts, std::vector<bgeot::size_type> &ind)
{
  ind.resize(npts);
  for (int i=0; i<npts; ++i)
  {
    ind[i] = pts[map_[i]];
  }
}

void copyMesh(vtkSmartPointer<vtkUnstructuredGrid> vtkGrid, getfem::mesh & gmesh)
{
  gmesh.clear();
  auto nnodes = vtkGrid->GetNumberOfPoints();
  std::vector<bgeot::size_type> ind(nnodes);
  for (auto i=0; i<nnodes; ++i)
  {
    double pt[3];
    vtkGrid->GetPoint(i, pt);
    ind[i] = gmesh.add_point(bgeot::base_node(pt[0], pt[1], pt[2]));
  }
  auto ncells = vtkGrid->GetNumberOfCells();
  auto cm = CellMapping();
  for (auto i=0; i<ncells; ++i)
  {
    vtkIdType npts;
    vtkIdType *pts = nullptr;
    auto cellType = vtkGrid->GetCellType(i);
    if (cellType == 1) continue;
    vtkGrid->GetCellPoints(i, npts, pts);
    cm.setup(cellType);
    cm.mapConnectivity(npts, pts, ind);
    gmesh.add_convex(cm.getTrans(), ind.begin());
  }
}

void interpolateField(const std::string &fieldName, vtkSmartPointer<vtkUnstructuredGrid> pMesh, const getfem::mesh_fem &fem, std::vector<double> &V) {

  using MeshPtr =   vtkSmartPointer<vtkUnstructuredGrid>;
  using PointsPtr = vtkSmartPointer<vtkPoints>;
  using ProbePtr = vtkSmartPointer<vtkProbeFilter>;
  using ArrayPtr = vtkSmartPointer<vtkDataArray>;

  auto pSampleMesh = MeshPtr::New();
  auto pNewPts = PointsPtr::New();
  double xyz[3] = {0,0,0};
  pNewPts->InsertNextPoint(xyz);
  pSampleMesh->SetPoints(pNewPts);

  pMesh->GetPointData()->SetActiveScalars(fieldName.c_str());

  auto pSampler = ProbePtr::New();
  pSampler->SetSourceData(pMesh);
  pSampler->SetInputData(pSampleMesh);
  pSampler->SetTolerance(0.00001);

  dal::bit_vector dofs = fem.basic_dof_on_region(getfem::mesh_region::all_convexes());
  V.resize(fem.nb_basic_dof()*fem.get_qdim(), 0.0);

  for (dal::bv_visitor dofId(dofs); !dofId.finished(); ++dofId) {
    auto point = fem.point_of_basic_dof(dofId);
    point.resize(3);
    pSampleMesh->GetPoints()->SetPoint(0, point[0], point[1], point[2]);
    pSampleMesh->Modified();
    pSampler->Update();
    MeshPtr  pResultMesh = pSampler->GetUnstructuredGridOutput();
    int dummy;
    ArrayPtr field = pResultMesh->GetPointData()->GetArray(fieldName.c_str(), dummy);
    ArrayPtr validator = pResultMesh->GetPointData()->GetArray(pSampler->GetValidPointMaskArrayName());
    bool v  = validator->GetComponent(0,0) > 0.5;
    if (v) {
      V[dofId] = field->GetComponent(0,fem.basic_dof_qdim(dofId));
    } else {
      std::stringstream oss;
      oss << "Interpolation from VTK file "
          << " at point " << point[0] << " " << point[1] << " " << point[2]
          << " FAILED";
      throw std::logic_error(oss.str());
    }
  }
}

std::string vectorsName(vtkSmartPointer<vtkUnstructuredGrid> vtkGrid) {
  return std::string(vtkGrid->GetPointData()->GetVectors()->GetName());
}

void readModel(getfem::mesh &mesh, getfem::model &model, const std::string &fileName) {
  using MyGridPtr = vtkSmartPointer<vtkUnstructuredGrid>;
  vtkUnstructuredGridReader *reader = vtkUnstructuredGridReader::New();
  reader->SetFileName(fileName.c_str());
  reader->ReadAllScalarsOn();
  reader->Update();
  vtkSmartPointer<vtkUnstructuredGrid> myGridPtr = reader->GetOutput();

  if (!model.variable_exists("u")) {
    throw std::logic_error("The model does not have variable 'u'");
  }

  copyMesh(myGridPtr, mesh);

  const getfem::mesh_fem &fem = model.mesh_fem_of_variable("u");
  auto &V = model.set_real_variable("u");
  std::string fieldName = vectorsName(myGridPtr);
  interpolateField(fieldName, myGridPtr, fem, V);
}

} // namespace v2g

