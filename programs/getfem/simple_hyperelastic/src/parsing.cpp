// Copyright (c) 2018 Roman Putanowicz

#include "parsing.h"
#include "errors.h"
#include <regex>
#include <string>

namespace sof {

int str2index(const std::string &str) {
  if (str == "X" || str == "x") {
    return 0;
  } else if (str == "Y" || str == "y") {
    return 1;
  } else if (str == "Z" || str == "z") {
    return 2;
  } else {
    return stoi(str);
  }
}
std::pair<std::string, int> parseNameIndex(const std::string &text) {
  static std::regex pieces_regex(
    "([a-zA-Z_]+[a-zA-Z0-9_]*)[ \t]*\\([\t ]*([0-9]+|[XxYyZz])[ \t]*\\)");
  std::smatch pieces_match;
  if (std::regex_match(text, pieces_match, pieces_regex)) {
     std::ssub_match nameMatch = pieces_match[1];
     std::ssub_match indexMatch = pieces_match[2];
     auto idx = str2index(indexMatch.str());
     return std::make_pair(nameMatch.str(), idx);
  } else {
    return std::make_pair(text, -1);
  }
}

} // namespace sof
