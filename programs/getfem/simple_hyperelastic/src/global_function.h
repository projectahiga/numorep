// Copyright (c) 2018 Roman Putanowicz
//! @brief Implement functor object that wrapps global functions.

#pragma once

#include <stdlib.h>
#include <muParser.h>
#include <array>
#include <array>

namespace sof
{

class VectorFunctionBase {
public:
  VectorFunctionBase(const std::string &name, size_t dim);
  virtual ~VectorFunctionBase() = default;
  std::vector<double> at(double x, double y, double z) const;
  std::vector<double> at(size_t ncoords, const double *coords) const;
  size_t dim() const;
  const std::string name() const;
  virtual std::string expression() const = 0;
protected:
  virtual std::vector<double> eval_() const = 0;
  mutable std::array<double, 3> pt_ = {{0.0, 0.0, 0.0}};
  size_t dim_= 1;
private:
  std::string name_ = std::string("unnamed");
};

class SimpleShearDisplacement : public VectorFunctionBase {
public:
  SimpleShearDisplacement(const std::string &name, double k, size_t dim=2);
  template <typename T>
  std::vector<double> operator ()(const T &point) const {
    return at(point[0], point[1], 0.0);
  }
  std::string expression() const;
private:
  double k_= 0.0;
  std::vector<double> eval_() const final override;
};

class ToCooksMembraneDisplacement : public VectorFunctionBase {
public:
  ToCooksMembraneDisplacement(double w, double h, double hl, double td);
  template <typename T>
  std::vector<double> operator ()(const T &point) const {
    return at(point[0], point[1], 0.0);
  }
  std::string expression() const;
private:
  double w_ = 1.0;
  double h_ = 1.0;
  double td_ = 0.0;
  double hl_ = 1.0;
  std::vector<double> eval_() const final override;
};


class GlobalVectorFunction : public VectorFunctionBase {
public:
  GlobalVectorFunction(const std::string &name, const std::string &expression);
  template <typename T>
  std::vector<double> operator ()(const T &point) const
  {
    pt_[0] = point[0];
    pt_[1] = point[1];
    pt_[2] = point[2];
    return eval_();
  }
  std::string expression() const {return parser_.GetExpr();}
private:
  mu::Parser parser_;
  std::vector<double> eval_() const final override;
};

class GlobalFunction
{
public:
  GlobalFunction(const std::string &name, const std::string &expression);
  double at(double x, double y, double z) const;
  double at(size_t ncoords, const double *coords) const;
  template <typename T>
  double operator ()(const T &point) const
  {
    pt_[0] = point[0];
    pt_[1] = point[1];
    pt_[2] = point[2];
    return parser_.Eval();
  }
  const std::string name() const {return name_;}
  const std::string expression() const {return parser_.GetExpr();}
private:
  std::string name_;
  mu::Parser parser_;
  mutable std::array<double, 3> pt_ = {{0.0, 0.0, 0.0}};
};

} // namespace sof
