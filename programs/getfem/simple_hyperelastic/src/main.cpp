#include <iostream>
#include <stdlib.h>

#include <easylogging++.h>

#include "manager.h"
#include "simple_model.h"
#include <getfem/getfem_config.h>
#include <gmm/gmm.h>

INITIALIZE_EASYLOGGINGPP

void main_body(const sof::Manager &myManager);
void setup_logging(const sof::Manager &myManager);
int selftest_main(int argc, char *argv[]);

int main(int argc, char *argv[])
{
 gmm::set_traces_level(0); // To avoid some trace messages
 GETFEM_MPI_INIT(argc, argv); // For parallelized version
 GMM_SET_EXCEPTION_DEBUG; // Exceptions make a memory fault, to debug.
 FE_ENABLE_EXCEPT;        // Enable floating point exception for Nan.

 sof::Manager myManager;
 if (myManager.parseCmdArgs(argc, argv))
 {
   if (myManager.do_testing()) {
     myManager.exitStatus = selftest_main(argc, argv);
   } else {
     setup_logging(myManager);
     main_body(myManager);
   }
 }
 GETFEM_MPI_FINALIZE;
 return myManager.exitStatus;
}

void setup_logging(const sof::Manager &myManager)
{
  if (myManager.noLog) {
    el::Configurations defaultConf;
    defaultConf.setToDefault();
    defaultConf.set(el::Level::Global, el::ConfigurationType::Enabled, std::string("false"));
    el::Loggers::reconfigureLogger("default", defaultConf);
  }
}

void display_listings(const sof::Manager &manager, const sof::SimpleModel &model) {
  if (manager.do_list("model")) model.listvar(std::cout);
  if (manager.do_list("dofs")) model.listdofs(std::cout);
  if (manager.do_list("variables")) model.listvariables(std::cout);
  if (manager.do_list("bricks")) model.listbricks(std::cout);
  if (manager.do_list("ranges")) model.listranges(std::cout, manager.rangesToShow());
}

void main_body(const sof::Manager &myManager) {
  LOG(INFO) << "Starting simulations" << std::endl;
  sof::SimpleModel model(myManager);
  auto request = myManager.request_type();
  if (request == "solve") {
    model.write_mesh();
    model.solve();
    model.write_solution();
  } else if (request == "analyze") {
    model.analyze();
    std::cout << "Probe points: " << std::endl;
    myManager.list_probe_points(std::cout);
  }
  display_listings(myManager, model);
  if (myManager.doEcho) {
    myManager.echo();
  }
}
