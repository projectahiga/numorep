#include "catch2/catch.hpp"
#include "parsing.h"

TEST_CASE( "Dummy test", "[dummy]" ) {
    REQUIRE( 1 == 1 );
}

TEST_CASE("Parse name(index)", "[parsing]") {
  auto niPair  = sof::parseNameIndex("u(2)");
  REQUIRE(std::string("u") == niPair.first);
  REQUIRE(2 == niPair.second);
  niPair  = sof::parseNameIndex("u ( 2 )");
  REQUIRE(std::string("u") == niPair.first);
  REQUIRE(2 == niPair.second);
  niPair  = sof::parseNameIndex("uU2(2)");
  REQUIRE(std::string("uU2") == niPair.first);
  REQUIRE(2 == niPair.second);
  REQUIRE(2 == niPair.second);
  niPair  = sof::parseNameIndex("u(X)");
  REQUIRE(std::string("u") == niPair.first);
  REQUIRE(0 == niPair.second);
  niPair  = sof::parseNameIndex("u(Y)");
  REQUIRE(std::string("u") == niPair.first);
  REQUIRE(1 == niPair.second);
  niPair  = sof::parseNameIndex("u(Z)");
  REQUIRE(std::string("u") == niPair.first);
  REQUIRE(2 == niPair.second);
  niPair  = sof::parseNameIndex("u");
  REQUIRE(std::string("u") == niPair.first);
  REQUIRE(-1 == niPair.second);
}
